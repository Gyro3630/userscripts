[← Homepage](../README.md)

# ProtonMail mobile fixes

Fix mobile-specific ProtonMail issues.

| [Install](../../../../raw/branch/master/protonmailMobileFixes/main.user.js) |
|-----------------------------------------------------------------------------|

- Move nav close button at nav open button location
- Display full email subject
- Add unread messages notifications