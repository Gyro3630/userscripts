// ==UserScript==
// @match       https://mail.proton.me/*
// @name        ProtonMail mobile fixes
// @description Fix mobile-specific ProtonMail issues
// @grant       GM.notification
// @version     0.3.0
// @author      KaKi87
// @license     GPL-3.0-or-later
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/protonmailMobileFixes
// ==/UserScript==

if(window.matchMedia('(hover: none)').matches){
    {
        const styles = new CSSStyleSheet();
        styles.replace(`
            /* Move nav close button at nav open button location */
            .logo-container div:nth-child(3) {
                order: -1;
            }
            /* Display full email subject */
            .text-ellipsis-two-lines {
                -webkit-line-clamp: inherit;
            }
        `);
        document.adoptedStyleSheets = [...document.adoptedStyleSheets, styles];
    }
    {
        // Add unread notifications
        let oldUnreadCount;
        new MutationObserver(() => {
            const [
                , unreadCount
                , folder
                , address
            ] = document.title.match(/^\(([0-9]+)\) (.+?) \| (.+?) \| Proton Mail$/) || [];
            if(
                !document.hasFocus()
                &&
                unreadCount
                &&
                unreadCount !== oldUnreadCount
            ){
                oldUnreadCount = unreadCount;
                GM.notification({
                    title: address,
                    text: `${unreadCount} unread message${unreadCount === '1' ? '' : 's'} in ${folder}`,
                    image: 'https://mail.proton.me/assets/favicon.ico'
                });
            }
        }).observe(
            document.querySelector('title'),
            {
                childList: true
            }
        );
    }
}