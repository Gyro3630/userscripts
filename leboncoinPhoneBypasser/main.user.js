// ==UserScript==
// @match       https://www.leboncoin.fr/*
// @name        LeBonCoin phone bypasser
// @description Bypass phone input on LeBonCoin
// @version     0.1.0
// @author      KaKi87
// @license     WTFPL
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/leboncoinPhoneBypasser
// ==/UserScript==

const _ = () => {
    const phoneInputElement = document.querySelector('#phone');
    if(phoneInputElement){
        let element = phoneInputElement;
        while(element.parentElement?.parentElement?.id !== 'mainContent'){
            if(element.parentElement)
                element = element.parentElement;
            else {
                console.error('Could not remove modal');
                return;
            }
        }
        element.remove();
        console.info('Modal removed');
    }
    setTimeout(_, 0);
};
_();