// ==UserScript==
// @match         https://main.elk.zone/*
// @name          Enhancements for Elk Zone
// @description   Enhancements for Elk Zone
// @grant         GM.getValue
// @grant         GM.setValue
// @grant         GM.deleteValue
// @version       0.2.2
// @author        KaKi87
// @license       MIT
// @namespace     https://git.kaki87.net/KaKi87/userscripts/src/branch/master/enhancementsForElkZone
// @run-at        document-start
// @require       https://git.kaki87.net/KaKi87/userscripts/raw/commit/8298d2543e28045dca302e1e3e0ed8f9fb840e0c/_lib/createFetchInterceptor.js
// ==/UserScript==

const
    postData = {},
    onPostData = {};

const { fetch } = unsafeWindow;

unsafeWindow.fetch = createFetchResponseInterceptor({
    fetch,
    onResponse: async ({
        url,
        response,
        resource
    }) => {
        if(url.startsWith('/')) return;
        const {
            pathname,
            hostname
        } = new URL(url);
        if(pathname.startsWith('/api/v1/statuses/')){
            if(!pathname.endsWith('/context')){
                const id = pathname.slice(17);
                postData[id] = await response.json();
                onPostData[id]?.();
                return;
            }
            const
                id = pathname.slice(17, -8),
                postContextData = await response.json();
            if(!postData[id])
                await new Promise(resolve => onPostData[id] = resolve);
            const
                {
                    pathname: remotePathname,
                    hostname: remoteHostname
                } = new URL(postData[id]['url']),
                remotePostId = remotePathname.split('/').slice(-1)[0],
                remotePostContextResponse = await fetch(`https://${remoteHostname}/api/v1/statuses/${remotePostId}/context`);
            if(remotePostContextResponse.status !== 200) return;
            const
                remotePostContextData = await remotePostContextResponse.json(),
                postUrls = [...postContextData['ancestors'], ...postContextData['descendants']].map(_ => _['url']),
                remotePostUrls = [...remotePostContextData['ancestors'], ...remotePostContextData['descendants']].map(_ => _['url']),
                unresolvedPostUrls = remotePostUrls.filter(url => !postUrls.includes(url)),
                unresolvedPostCount = unresolvedPostUrls.length;
            if(!unresolvedPostCount) return;
            for(const postUrl of unresolvedPostUrls){
                const url = new URL(`https://${hostname}/api/v2/search`);
                url.searchParams.set('q', postUrl);
                url.searchParams.set('type', 'statuses');
                url.searchParams.set('resolve', 'true');
                await fetch(
                    url,
                    {
                        headers: {
                            'authorization': resource.headers.get('authorization')
                        }
                    }
                );
            }
            return fetch(resource);
        }
        if(
            pathname === '/api/v1/timelines/home'
            ||
            pathname === '/api/v1/trends/statuses'
            ||
            pathname.startsWith('/api/v1/accounts') && pathname.endsWith('/statuses')
            ||
            pathname.startsWith('/api/v1/timelines/list/')
        ){
            const postFilter = await GM.getValue('postFilter');
            if(!postFilter) return;
            let
                posts,
                filteredPosts,
                lastPostId;
            do {
                if(posts){
                    const _lastPostId = posts.slice(-1)[0].id;
                    if(lastPostId && lastPostId === _lastPostId) break;
                    lastPostId = _lastPostId;
                    const postsUrl = new URL(url);
                    postsUrl.searchParams.set('max_id', _lastPostId);
                    response = await fetch(new Request(postsUrl, resource));
                }
                posts = await response.json();
                filteredPosts = posts.filter(post =>
                    (typeof postFilter.reposts !== 'boolean' || postFilter.reposts === !!post['reblog'])
                    &&
                    (typeof postFilter.attachments !== 'boolean' || postFilter.attachments === !!post['media_attachments'].length)
                    &&
                    (typeof postFilter.cards !== 'boolean' || postFilter.cards === !!post['card'])
                    &&
                    (!Array.isArray(postFilter.languages) || postFilter.languages.includes(post['language']))
                );
            }
            while(posts.length && !filteredPosts.length);
            return new Response(
                JSON.stringify(filteredPosts),
                response
            );
        }
    }
});

unsafeWindow.__enhancementsForElkZone__ = {
    setPostFilter: ({
        reposts,
        attachments,
        cards,
        languages
    }) => GM.setValue(
        'postFilter',
        {
            reposts,
            attachments,
            cards,
            languages
        }
    ),
    unsetPostFilter: () => GM.deleteValue('postFilter')
};