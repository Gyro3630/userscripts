/* ========================
SLIX.IO ASSISTANT by KaKi87
		v1.2.0 (21/06/17)
======================== */

console.clear();

document.body.appendChild(copyright = document.createElement("p"));
copyright.style.position = "absolute";
copyright.style.bottom = "0";
copyright.style.right = "5px";
copyright.style.backgroundColor = "white";
copyright.style.color = "black";
copyright.style.padding = "3px";
copyright.innerText = "Splix Assistant Script v1.0.1-stable © KaKi87";

var playing;
setInterval(function(){
	playing = document.getElementById("beginScreen").style.display == "none";
	if(playing){
		Playing();
	}
	else {
		stop();
	}
}, 100);

function Playing(){ // All things to do when playing
	displayLocation();
	if(grid.style.display == "none")
		grid.style.display = "block";
	if(document.getElementById("lifeBox") !== null){
		document.getElementById("lifeBox").style.top = "275px";
		document.getElementById("lifeBox").style.left = "55px";
	}
}

function stop(){ // All things to do when game over
	autoPilot.abort();
	lastActions = [];
	if(monitoringLocation)
		myLocation();
	if(typeof(el) !== "undefined" && el.style.display !== "none")
		el.style.display = "none";
	grid.style.display = "none";
}

function displayLocation(){
	if(typeof(el) == "undefined"){
		document.body.appendChild(el = document.createElement("div"));
		el.style.position = "absolute";
		el.style.bottom = "0";
		el.style.left = "50%";
		el.style.transform = "translateX(-50%)";
		el.style.backgroundColor = "white";
		el.style.padding = "10px";
	}
	else if(el.style.display == "none")
		el.style.display = "block";
	el.innerText = Math.round(myPlayer.pos[0]) + " " + Math.round(myPlayer.pos[1]);
}

var dirUp 	 = 3,
	dirRight = 0,
	dirDown  = 1,
	dirLeft  = 2;

var lastActions = [];

function up(){
	if(getDirection() !== "U" && myPlayer.pos[1] > 1){
		if(sendDir(dirUp)){
			// console.log("U");
			if(lastActions[lastActions.length - 1] !== "U")
				lastActions[lastActions.length] = "U";
		}
		else {
			// console.log("U !")
			if(getDirection() == "D"){
				// console.log("Last action : " + lastActions[lastActions.length - 2]);
				switch(lastActions[lastActions.length - 2]){
					case "L" :
						right();
						setTimeout(function(){ up(); }, 100);
						break;
					case "R" :
						left();
						setTimeout(function(){ up(); }, 100);
						break;
					default :
						right();
						setTimeout(function(){ up(); }, 100);
						break;
				}
			}
		}
	}
}
function right(){
	if(getDirection() !== "R" && myPlayer.pos[0] < 598){
		if(sendDir(dirRight)){
			// console.log("R");
			if(lastActions[lastActions.length - 1] !== "R")
				lastActions[lastActions.length] = "R";
		}
		else {
			// console.log("R !");
			if(getDirection() == "L"){
				// console.log("Last action : " + lastActions[lastActions.length - 2]);
				switch(lastActions[lastActions.length - 2]){
					case "D" :
						up();
						setTimeout(function(){ right(); }, 100);
						break;
					case "U" :
						down();
						setTimeout(function(){ right(); }, 100);
						break;
					default :
						up();
						setTimeout(function(){ right(); }, 100);
						break;
				}
			}
		}
	}
}
function down(){
	if(getDirection() !== "D" && myPlayer.pos[1] < 598){
		if(sendDir(dirDown)){
			// console.log("D");
			if(lastActions[lastActions.length - 1] !== "D")
				lastActions[lastActions.length] = "D";
		}
		else {
			// console.log("D !");
			if(getDirection() == "U"){
				// console.log("Last action : " + lastActions[lastActions.length - 2]);
				switch(lastActions[lastActions.length - 2]){
					case "L" :
						right();
						setTimeout(function(){ down(); }, 100);
						break;
					case "R" :
						left();
						setTimeout(function(){ down(); }, 100);
						break;
					default :
						right();
						setTimeout(function(){ down(); }, 100);
						break;
				}
			}
		}
	}
}
function left(){
	if(getDirection() !== "L" && myPlayer.pos[0] > 1){
		if(sendDir(dirLeft)){
			// console.log("L");
			if(lastActions[lastActions.length - 1] !== "L")
				lastActions[lastActions.length] = "L";
		}
		else {
			// console.log("L !");
			if(getDirection() == "R"){
				// console.log("Last action : " + lastActions[lastActions.length - 2]);
				switch(lastActions[lastActions.length - 2]){
					case "D" :
						up();
						setTimeout(function(){ left(); }, 100);
						break;
					case "U" :
						down();
						setTimeout(function(){ left(); }, 100);
						break;
					default :
						up();
						setTimeout(function(){ left(); }, 100);
						break;
				}
			}
		}
	}
}

var paused = false;
var pauseLoop;
function pause(){
	if(!paused){
		pauseLoop = setInterval(function(){
			sendDir((myPlayer.dir + 1) % 4);
		}, 100);
		console.log("Pause");
	}
	else {
		clearInterval(pauseLoop);
		console.log("Play");
	}
}

function suicideV2(){
	var suicideLoop = setInterval(function(){
		sendDir((myPlayer.dir + 1) % 4);
	}, 100);
}

function suicide(){
	switch(getDirection()){
		case "U" :
			right();
			setTimeout(down, 100);
			setTimeout(left, 200);
			break;
		case "D" :
			left();
			setTimeout(up, 100);
			setTimeout(right, 200);
			break;
		case "L" :
			up();
			setTimeout(right, 100);
			setTimeout(down, 200);
			break;
		case "R" :
			down();
			setTimeout(left, 100);
			setTimeout(up, 200);
			break;
	}
}

function getDirection(){
	switch(myPlayer.dir){
		case dirUp :
			return "U";
			break;
		case dirRight :
			return "R";
			break;
		case dirDown :
			return "D";
			break;
		case dirLeft :
			return "L";
			break;
	}
}

var monitoringLocation = false;
var monitoringLoop;
function myLocation(){
	if(!monitoringLocation){
		monitoringLocation = true;
		monitoringLoop = setInterval(function(){
			console.log(Math.round(myPlayer.pos[0]) + " " + Math.round(myPlayer.pos[1]));
		}, 500);
	}
	else {
		clearInterval(monitoringLoop);
		monitoringLocation = false;
	}
}

function findPlayer(playerName){
	if(typeof(playerName) == "string"){
		for(var i = 0; i < players.length; i++){
			if(players[i].name == playerName)
				return players[i];
		}
	}
}

function listPlayers(){
	if(players.length > 1){
		if(players.length > 2)
			console.log(players.length - 1 + " players in local area");
		else
			console.log(players.length - 1 + " player in local area");
	    for(var i = 1; i < players.length; i++){
	        console.log(players[i].name + " (" + playerDistance(players[i].name) + ")");
	    }
	}
	else
		console.log("You're alone.");
}

function playerDistance(playerName){
	if(typeof(playerName) == "string"){
		// return Math.abs(Math.round(myPlayer.pos[0] - findPlayer(playerName).pos[0] + myPlayer.pos[1] - findPlayer(playerName).pos[1]));
		var a = myPlayer.pos[0] - findPlayer(playerName).pos[0];
		var b = myPlayer.pos[1] - findPlayer(playerName).pos[1]
		var c = Math.round(Math.sqrt(a*a+b*b));
		return c;
	}
}

function nearestPlayer(requiredData){
	if(players.length > 1){
		var playersArray = [];
		var distanceArray = [];
		for(var i = 1; i < players.length; i++){
			playersArray[i-1] = players[i].name;
			distanceArray[i-1] = playerDistance(players[i].name);
			if(i == players.length - 1){
				var nearestPlayerDistance = Math.min.apply(Math, distanceArray);
				var nearestPlayerName = playersArray[distanceArray.indexOf(nearestPlayerDistance)];
				if(requiredData !== undefined){
					if(requiredData == "playerName")
						return nearestPlayerName;
					else if(requiredData == "playerDistance")
						return nearestPlayerDistance;
				}
				else
					console.log("Nearest player : " + playersArray[distanceArray.indexOf(nearestPlayerDistance)] + " (" + nearestPlayerDistance + ")");
			}
		}
	}
	else
		console.log("No player near you.");
}


var autoPilot = {};

autoPilot.goTo = function(x, y){
	console.log("Current coordinates : " + myPlayer.pos[0] + " " + myPlayer.pos[1]);
	console.log("Targeting coordinates : " + x + " " + y);
	var event = new Event("goToSucceed");
	if(x >= 1 && x <= 598 && y >= 1 && y <= 598 && typeof(x) == "number" && typeof(y) == "number"){
		if(myPlayer.pos[0] > x){
			console.log("Going left");
			left();
			waitX();
		}
		else if(myPlayer.pos[0] < x){
			console.log("Going right");
			right();
			waitX();
		}
		else
			Y();
		function waitX(){
			var goToX = setInterval(function(){
				if(Math.abs(myPlayer.pos[0] - x) < 1){
					clearInterval(goToX);
					console.log("X coordinates reached");
					Y();
				}
			}, 100);
		}
		function Y(){
			if(myPlayer.pos[1] > y){
				console.log("Going up");
				up();
				waitY();
			}
			else if(myPlayer.pos[1] < y){
				console.log("Going down");
				down();
				waitY();
			}
			function waitY(){
				var goToY = setInterval(function(){
					if(Math.abs(myPlayer.pos[1] - y) < 1){
						clearInterval(goToY);
						console.log("Y coordinates reached");
						document.dispatchEvent(event);
					}
				}, 100);
			}
		}
	}
	else {
		console.log("Bad coordinates");
		return false;
	}
}

autoPilot.makeSquare = function(width, height, directions){
	if(typeof(width) == "number" && typeof(height) == "number"
	&& width <= 598 && height <= 598
	&& typeof(directions) == "string" && directions.length == 2){
		var stage = 1;
		if(directions.toLowerCase()[0] == "u"){
			autoPilot.goTo(myPlayer.pos[0], myPlayer.pos[1] - height);
			stage++;
			document.addEventListener("goToSucceed", function(){
				if(stage == 2){
					switch(directions.toLowerCase()[1]){
						case "l" :
							autoPilot.goTo(myPlayer.pos[0] - width, myPlayer.pos[1]);
							break;
						case "r" :
							autoPilot.goTo(myPlayer.pos[0] + width, myPlayer.pos[1]);
							break;
					}
					stage++;
				}
				else if(stage == 3){
					autoPilot.goTo(myPlayer.pos[0], myPlayer.pos[1] + height);
				}
			}, false);
		}
		else if(directions.toLowerCase()[0] == "d"){
			autoPilot.goTo(myPlayer.pos[0], myPlayer.pos[1] + height);
			stage++
			document.addEventListener("goToSucceed", function(){
				if(stage == 2){
					switch(directions.toLowerCase()[1]){
						case "l" :
							autoPilot.goTo(myPlayer.pos[0] - width, myPlayer.pos[1]);
							break;
						case "r" :
							autoPilot.goTo(myPlayer.pos[0] + width, myPlayer.pos[1]);
							break;
					}
					stage++;
				}
				else if(stage == 3){
					autoPilot.goTo(myPlayer.pos[0], myPlayer.pos[1] - height);
				}
			}, false);
		}
	}
	else
		console.log("Bad parameters");
}

autoPilot.engage = function(){
	//
}

var antiBorderKill = setInterval(function(){
	if(playing && myPlayer !== null){
		if((myPlayer.pos[0] < 1 || myPlayer.pos[0] > 598) // Borders left/right
			&& (getDirection() == "L" || getDirection() == "R")){
			switch(lastActions[lastActions.length - 2]){
				case "U" :
					down();
					break;
				case "D" :
					up();
					break;
				default :
					down();
					break;
			}
		}
		else if((myPlayer.pos[1] < 1 || myPlayer.pos[1] > 598) // Borders top/bottom
				&& (getDirection() == "U" || getDirection() == "D")){
			switch(lastActions[lastActions.length - 2]){
				case "L" :
					right();
					break;
				case "R" :
					left();
					break;
				default :
					right();
					break;
			}
		}
	}
}, 1);

/*

	Playground dimensions : 598 x 598
	Extreme coordinates   : 1 - 598

*/

function improveMinimap(){
	var minimap = document.getElementById("miniMap");
	minimap.style.transform = "scale(1.4)";
	minimap.style.opacity = "0.7";
	document.body.appendChild(grid = document.createElement("div"));
	grid.style.display = "none";
	grid.style.position = "absolute";
	grid.style.top = "16px";
	grid.style.left = "16px";
	grid.appendChild(gridImg = document.createElement("img"));
	gridImg.src = "https://kaki87.net/grid-letters-min.png";
	gridImg.style.width = "240px";
	gridImg.style.height = "240px";
}

improveMinimap();

/* ===== KEYBOARD INTERACTIONS ===== */

var keyboardMap = [
	"", // [0]
	"", // [1]
	"", // [2]
	"CANCEL", // [3]
	"", // [4]
	"", // [5]
	"HELP", // [6]
	"", // [7]
	"BACK_SPACE", // [8]
	"TAB", // [9]
	"", // [10]
	"", // [11]
	"CLEAR", // [12]
	"ENTER", // [13]
	"ENTER_SPECIAL", // [14]
	"", // [15]
	"SHIFT", // [16]
	"CONTROL", // [17]
	"ALT", // [18]
	"PAUSE", // [19]
	"CAPS_LOCK", // [20]
	"KANA", // [21]
	"EISU", // [22]
	"JUNJA", // [23]
	"FINAL", // [24]
	"HANJA", // [25]
	"", // [26]
	"ESCAPE", // [27]
	"CONVERT", // [28]
	"NONCONVERT", // [29]
	"ACCEPT", // [30]
	"MODECHANGE", // [31]
	"SPACE", // [32]
	"PAGE_UP", // [33]
	"PAGE_DOWN", // [34]
	"END", // [35]
	"HOME", // [36]
	"LEFT", // [37]
	"UP", // [38]
	"RIGHT", // [39]
	"DOWN", // [40]
	"SELECT", // [41]
	"PRINT", // [42]
	"EXECUTE", // [43]
	"PRINTSCREEN", // [44]
	"INSERT", // [45]
	"DELETE", // [46]
	"", // [47]
	"0", // [48]
	"1", // [49]
	"2", // [50]
	"3", // [51]
	"4", // [52]
	"5", // [53]
	"6", // [54]
	"7", // [55]
	"8", // [56]
	"9", // [57]
	"COLON", // [58]
	"SEMICOLON", // [59]
	"LESS_THAN", // [60]
	"EQUALS", // [61]
	"GREATER_THAN", // [62]
	"QUESTION_MARK", // [63]
	"AT", // [64]
	"A", // [65]
	"B", // [66]
	"C", // [67]
	"D", // [68]
	"E", // [69]
	"F", // [70]
	"G", // [71]
	"H", // [72]
	"I", // [73]
	"J", // [74]
	"K", // [75]
	"L", // [76]
	"M", // [77]
	"N", // [78]
	"O", // [79]
	"P", // [80]
	"Q", // [81]
	"R", // [82]
	"S", // [83]
	"T", // [84]
	"U", // [85]
	"V", // [86]
	"W", // [87]
	"X", // [88]
	"Y", // [89]
	"Z", // [90]
	"OS_KEY", // [91] Windows Key (Windows) or Command Key (Mac)
	"", // [92]
	"CONTEXT_MENU", // [93]
	"", // [94]
	"SLEEP", // [95]
	"NUMPAD0", // [96]
	"NUMPAD1", // [97]
	"NUMPAD2", // [98]
	"NUMPAD3", // [99]
	"NUMPAD4", // [100]
	"NUMPAD5", // [101]
	"NUMPAD6", // [102]
	"NUMPAD7", // [103]
	"NUMPAD8", // [104]
	"NUMPAD9", // [105]
	"MULTIPLY", // [106]
	"ADD", // [107]
	"SEPARATOR", // [108]
	"SUBTRACT", // [109]
	"DECIMAL", // [110]
	"DIVIDE", // [111]
	"F1", // [112]
	"F2", // [113]
	"F3", // [114]
	"F4", // [115]
	"F5", // [116]
	"F6", // [117]
	"F7", // [118]
	"F8", // [119]
	"F9", // [120]
	"F10", // [121]
	"F11", // [122]
	"F12", // [123]
	"F13", // [124]
	"F14", // [125]
	"F15", // [126]
	"F16", // [127]
	"F17", // [128]
	"F18", // [129]
	"F19", // [130]
	"F20", // [131]
	"F21", // [132]
	"F22", // [133]
	"F23", // [134]
	"F24", // [135]
	"", // [136]
	"", // [137]
	"", // [138]
	"", // [139]
	"", // [140]
	"", // [141]
	"", // [142]
	"", // [143]
	"NUM_LOCK", // [144]
	"SCROLL_LOCK", // [145]
	"WIN_OEM_FJ_JISHO", // [146]
	"WIN_OEM_FJ_MASSHOU", // [147]
	"WIN_OEM_FJ_TOUROKU", // [148]
	"WIN_OEM_FJ_LOYA", // [149]
	"WIN_OEM_FJ_ROYA", // [150]
	"", // [151]
	"", // [152]
	"", // [153]
	"", // [154]
	"", // [155]
	"", // [156]
	"", // [157]
	"", // [158]
	"", // [159]
	"CIRCUMFLEX", // [160]
	"EXCLAMATION", // [161]
	"DOUBLE_QUOTE", // [162]
	"HASH", // [163]
	"DOLLAR", // [164]
	"PERCENT", // [165]
	"AMPERSAND", // [166]
	"UNDERSCORE", // [167]
	"OPEN_PAREN", // [168]
	"CLOSE_PAREN", // [169]
	"ASTERISK", // [170]
	"PLUS", // [171]
	"PIPE", // [172]
	"HYPHEN_MINUS", // [173]
	"OPEN_CURLY_BRACKET", // [174]
	"CLOSE_CURLY_BRACKET", // [175]
	"TILDE", // [176]
	"", // [177]
	"", // [178]
	"", // [179]
	"", // [180]
	"VOLUME_MUTE", // [181]
	"VOLUME_DOWN", // [182]
	"VOLUME_UP", // [183]
	"", // [184]
	"", // [185]
	"SEMICOLON", // [186]
	"EQUALS", // [187]
	"COMMA", // [188]
	"MINUS", // [189]
	"PERIOD", // [190]
	"SLASH", // [191]
	"BACK_QUOTE", // [192]
	"", // [193]
	"", // [194]
	"", // [195]
	"", // [196]
	"", // [197]
	"", // [198]
	"", // [199]
	"", // [200]
	"", // [201]
	"", // [202]
	"", // [203]
	"", // [204]
	"", // [205]
	"", // [206]
	"", // [207]
	"", // [208]
	"", // [209]
	"", // [210]
	"", // [211]
	"", // [212]
	"", // [213]
	"", // [214]
	"", // [215]
	"", // [216]
	"", // [217]
	"", // [218]
	"OPEN_BRACKET", // [219]
	"BACK_SLASH", // [220]
	"CLOSE_BRACKET", // [221]
	"QUOTE", // [222]
	"", // [223]
	"META", // [224]
	"ALTGR", // [225]
	"", // [226]
	"WIN_ICO_HELP", // [227]
	"WIN_ICO_00", // [228]
	"", // [229]
	"WIN_ICO_CLEAR", // [230]
	"", // [231]
	"", // [232]
	"WIN_OEM_RESET", // [233]
	"WIN_OEM_JUMP", // [234]
	"WIN_OEM_PA1", // [235]
	"WIN_OEM_PA2", // [236]
	"WIN_OEM_PA3", // [237]
	"WIN_OEM_WSCTRL", // [238]
	"WIN_OEM_CUSEL", // [239]
	"WIN_OEM_ATTN", // [240]
	"WIN_OEM_FINISH", // [241]
	"WIN_OEM_COPY", // [242]
	"WIN_OEM_AUTO", // [243]
	"WIN_OEM_ENLW", // [244]
	"WIN_OEM_BACKTAB", // [245]
	"ATTN", // [246]
	"CRSEL", // [247]
	"EXSEL", // [248]
	"EREOF", // [249]
	"PLAY", // [250]
	"ZOOM", // [251]
	"", // [252]
	"PA1", // [253]
	"WIN_OEM_CLEAR", // [254]
	"" // [255]
];

document.body.onkeydown = function(){
	switch(keyboardMap[event.keyCode]){
		case "NUMPAD8" :
			up();
			break;
		case "NUMPAD6" :
			right();
			break;
		case "NUMPAD5" :
			down();
			break;
		case "NUMPAD2" :
			down();
			break;
		case "NUMPAD4" :
			left();
			break;
		case "NUMPAD0" :
			suicide();
			break;
		case "MULTIPLY" :
			pause();
			break;
	}
}