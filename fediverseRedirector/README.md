[← Homepage](../README.md)

# Fediverse Redirector

Redirect any Fediverse app to your favourite one.

| [Install](../../../../raw/branch/master/fediverseRedirector/main.user.js) |
|---------------------------------------------------------------------------|

## Supported apps

|                                                            | Mastodon           | Elk                              | Lemmy              | Pixelfed                         | Kbin<sup>(1)</sup> | Alexandrite        | Photon | Firefish |
|------------------------------------------------------------|--------------------|----------------------------------|--------------------|----------------------------------|--------------------|--------------------|--------|----------|
| [Mastodon](https://github.com/mastodon/mastodon)           | :white_check_mark: | :white_check_mark:               |                    | :white_check_mark:               |                    |                    |        | TBA      |
| [Elk](https://github.com/elk-zone/elk)                     | :white_check_mark: | :white_check_mark:               |                    | :white_check_mark:<sup>(2)</sup> |                    |                    |        | TBA      |
| [Lemmy](https://github.com/LemmyNet/lemmy-ui)              |                    |                                  | :white_check_mark: |                                  | :white_check_mark: | :white_check_mark: | TBA    |          |
| [Pixelfed](https://github.com/pixelfed/pixelfed)           | :white_check_mark: | :white_check_mark:<sup>(3)</sup> |                    | :white_check_mark:               |                    |                    |        | TBA      |
| [Kbin](https://github.com/ernestwisniewski/kbin)           |                    |                                  | :white_check_mark: |                                  | :white_check_mark: |                    |        |          |
| [Alexandrite](https://github.com/sheodox/alexandrite)      |                    |                                  | :white_check_mark: |                                  | TBA                | :white_check_mark: | TBA    |          |
| [Photon](https://github.com/Xyphyn/photon)                 |                    |                                  | TBA                |                                  | TBA                | TBA                | TBA    |          |
| [Firefish](https://git.joinfirefish.org/firefish/firefish) | TBA                | TBA                              |                    | TBA                              |                    |                    |        | TBA      |

### Exceptions & warnings

1. Posts & comments redirection to Kbin is unsupported (cf. [`Kbin/kbin-core@codeberg.org#497`](https://codeberg.org/Kbin/kbin-core/issues/497))
2. Post redirection to Pixelfed requires Mastodon -> Pixelfed redirection to be enabled
3. Post redirection to Elk requires Mastodon -> Elk redirection to be enabled

## Usage

To set your favourite instances, open *Settings* from the userscript menu :

![](./screenshot1.png)

![](./screenshot2.png)

## Changelog

* `0.1.0` (2023-06-28) • [Initial release](https://git.kaki87.net/KaKi87/userscripts/src/commit/2786047b68d587f3eb951e9d56dc8d7636723b04/fediverseRedirector)
* `0.1.1` (2023-06-29) • [Fix unhandled query params preventing matching](https://git.kaki87.net/KaKi87/userscripts/src/commit/bea60cfa923bab15aa76882a4e76b6a40c150c57/fediverseRedirector)
* `0.1.2` (2023-06-29) • [Fix unsupported nested communities for post redirection, add Lemmy comment redirection](https://git.kaki87.net/KaKi87/userscripts/src/commit/5c1a7e6ac801094c598762d8328c2adcf81c77a4/fediverseRedirector)
* `0.2.0` (2023-07-13) • [Rewrite settings UI & redirection algorithm](https://git.kaki87.net/KaKi87/userscripts/src/commit/32aacf67ed1abe626555dd257a728a1c039e9aea/fediverseRedirector)
* `0.2.1` (2023-07-30)
  * Add Pixelfed <-> Mastodon redirection
  * Add Pixelfed <-> Elk redirection
  * Add Kbin redirection
  * Add Kbin <-> Lemmy redirection