// ==UserScript==
// @match         https://chat.reddit.com/*
// @name          Enhancements for Reddit Chat
// @description   Responsive design & notifications for Reddit chat
// @grant         GM.notification
// @version       0.2.1
// @author        KaKi87
// @license       MIT
// @namespace     https://git.kaki87.net/KaKi87/userscripts/src/branch/master/redditOldBetterChat
// ==/UserScript==

const
    styles1 = new CSSStyleSheet(),
    styles2 = new CSSStyleSheet();
// language=CSS
styles1.replaceSync(`
    body {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    rs-app {
        height: 100%;
    }
`);
document.adoptedStyleSheets = [...document.adoptedStyleSheets, styles1];
console.log(document.adoptedStyleSheets);
// language=CSS
styles2.replaceSync(`
    .container {
        grid-template-columns: 1fr;
        grid-template-rows: 1fr 2fr;
    }
`);
const observer = new MutationObserver(() => {
    const _document = document.querySelector('rs-app').shadowRoot;
    if(_document){
        observer.disconnect();
        _document.adoptedStyleSheets = [..._document.adoptedStyleSheets, styles2];
    }
});
observer.observe(
    document.documentElement,
    {
        childList: true,
        subtree: true
    }
);