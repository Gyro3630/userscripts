// ==UserScript==
// @match         https://old.reddit.com/*
// @match         https://www.redditmedia.com/mediaembed/*
// @match         https://chat.reddit.com/*
// @name          Responsive design for Reddit Old
// @description   Mobile-friendly design for old.reddit.com
// @version       0.2.1
// @author        KaKi87
// @license       MIT
// @namespace     https://git.kaki87.net/KaKi87/userscripts/src/branch/master/redditOldResponsive
// ==/UserScript==

if(window.location.hostname === 'old.reddit.com'){
    {
        const metaViewportElement = document.createElement('meta');
        metaViewportElement.setAttribute('name', 'viewport');
        metaViewportElement.setAttribute('content', 'width=device-width, initial-scale=1');
        document.head.appendChild(metaViewportElement);
    }

    {
        const styles = new CSSStyleSheet();
        // language=CSS
        styles.replaceSync(`
            html,
            body {
                overflow-x: hidden;
            }
            .listing-chooser,
            #redesign-beta-optin-btn {
                display: none;
            }
            #sr-header-area {
                height: auto;
            }
            #sr-header-area .width-clip {
                display: flex;
            }
            #sr-header-area .width-clip,
            #sr-header-area .sr-list,
            #sr-more-link,
            #header-bottom-right {
                position: inherit;
            }
            #sr-header-area .sr-list {
                overflow: auto;        
            }
            .tabmenu {
                max-width: 100%;
                overflow: auto;
            }
            .side {
                z-index: 101;
                position: absolute;
                top: 0;
                left: 0;
                margin: var(--header-height) 0 0 0;
                width: 0;
                overflow-x: hidden;
            }
            .side--active {
                width: 100%;
            }
            .side--active ~ .content {
                height: var(--sidebar-height);
            }
            .content {
                margin: 0 !important;
                width: 100% !important;
                overflow-x: auto;
                overflow-y: hidden;
            }
            .comments-page #siteTable .thing {
                display: grid;
                grid-template-columns: auto 1fr;
                grid-template-areas:
                    'score thumbnail'
                    'main  main';
                grid-gap: 5px;
            }
            .comments-page #siteTable .thing:not(:has(.thumbnail)) {
                grid-template-areas:
                    'score main'
                    '.     main';
            }
            .comments-page #siteTable .thing .midcol {
                grid-area: score;
            }
            .comments-page #siteTable .thing .thumbnail {
                grid-area: thumbnail;
            }
            .comments-page #siteTable .thing .entry {
                grid-area: main;
            }
            .comments-page #siteTable .thing .media-embed {
                width: 100%;
                height: auto;
            }
            .infobar,
            .searchpane,
            .wiki-page .wiki-page-content,
            .panestack-title,
            .commentarea .menuarea {
                margin: 5px;
            }
            .commentarea .menuarea {
                display: flex;
            }
            #search input[type=text],
            .roundfield,
            .formtabs-content {
                width: 100%;
            }
            .subreddit .midcol {
                width: auto !important;
            }
            .roundfield,
            .roundfield * {
                box-sizing: border-box;
            }
            .roundfield input,
            .roundfield textarea,
            .roundfield select {
                max-width: 100%;
            }
            .usertext,
            .menuarea {
                overflow: auto;
            }
            .combined-search-page #search {
                padding: 0;
                display: grid;
                grid-template-columns: 1fr auto;
                grid-template-areas:
                    'query             submit'
                    'advanced          advanced'
                    'restrictSubreddit restrictSubreddit'
                    'nsfw              nsfw';
            }
            .combined-search-page #search input[type=text] {
                grid-area: query;
                min-width: auto;
            }
            .combined-search-page #search .search-submit-button {
                grid-area: submit;
            }
            .combined-search-page #search label:has([name="restrict_sr"]) {
                grid-area: restrictSubreddit;
            }
            .combined-search-page #search label:has([name="include_over_18"]) {
                grid-area: nsfw;
            }
            .combined-search-page #search p:has(#search_showmore) {
                grid-area: advanced;
            }
            .search-result-group {
                width: 100% !important;
                min-width: auto !important;
                box-sizing: border-box;
            }
            .search-result-subreddit .search-result-meta {
                padding: 0.5rem 0;
                display: flex;
                flex-direction: column;
                row-gap: 0.5rem;
            }
            .search-result-subreddit .search-result-meta .stamp {
                align-self: flex-start;
            }
            .search-link,
            .search-result-body a {
                overflow-wrap: anywhere;
                white-space: normal;
            }
            .chat-app-window.regular {
                width: 100%;
                height: 100%;
                left: 0;
            }
            .author-tooltip {
                left: 0 !important;
                right: auto !important;
                width: 100%;
                box-sizing: border-box;
            }
        `);
        document.adoptedStyleSheets = [...document.adoptedStyleSheets, styles];
    }

    {
        const sidebarElement = document.querySelector('.side');
        if(sidebarElement){
            const
                menuElement = document.querySelector('.tabmenu'),
                sidebarButtonContainerElement = document.createElement('li'),
                sidebarButtonElement = document.createElement('a');
            sidebarButtonElement.textContent = 'sidebar';
            sidebarButtonElement.setAttribute('href', '#');
            sidebarButtonElement.addEventListener(
                'click',
                event => {
                    event.preventDefault();
                    const extraHeaderHeight = document.querySelector('.submit-page')
                        ? window.$('.content > h1').outerHeight(true) + window.$(menuElement).outerHeight(true) + 10
                        : 0;
                    document.documentElement.style.setProperty(
                        '--header-height',
                        `${window.$('#header').outerHeight(true) + extraHeaderHeight}px`
                    );
                    sidebarButtonContainerElement.classList.toggle(
                        'selected',
                        sidebarElement.classList.toggle('side--active')
                    );
                    document.documentElement.style.setProperty(
                        '--sidebar-height',
                        `${sidebarElement.offsetHeight + extraHeaderHeight}px`
                    );
                }
            );
            sidebarButtonContainerElement.appendChild(sidebarButtonElement);
            menuElement.appendChild(sidebarButtonContainerElement);
        }
    }
}
else if(window.location.hostname === 'www.redditmedia.com'){
    const styles = new CSSStyleSheet();
    // language=CSS
    styles.replaceSync(`
        .embedly-embed {
            width: 100%;
        }
    `);
    document.adoptedStyleSheets = [...document.adoptedStyleSheets, styles];
}
else if(window.location.hostname === 'chat.reddit.com'){
    const
        styles1 = new CSSStyleSheet(),
        styles2 = new CSSStyleSheet();
    // language=CSS
    styles1.replaceSync(`
        body {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
        rs-app {
            height: 100%;
        }
    `);
    document.adoptedStyleSheets = [...document.adoptedStyleSheets, styles1];
    console.log(document.adoptedStyleSheets);
    // language=CSS
    styles2.replaceSync(`
        .container {
            grid-template-columns: 1fr;
            grid-template-rows: 1fr 2fr;
        }
    `);
    const observer = new MutationObserver(() => {
        const _document = document.querySelector('rs-app').shadowRoot;
        if(_document){
            observer.disconnect();
            _document.adoptedStyleSheets = [..._document.adoptedStyleSheets, styles2];
        }
    });
    observer.observe(
        document.documentElement,
        {
            childList: true,
            subtree: true
        }
    );
}