[← Homepage](../README.md)

# Responsive design for Reddit Old

Mobile-friendly design for [old.reddit.com](https://old.reddit.com).

| [Install](../../../../raw/branch/master/redditOldResponsive/main.user.js) |
|---------------------------------------------------------------------------|

| ![](./screenshot1.png) | ![](./screenshot2.png) |
|------------------------|------------------------|
| ![](./screenshot3.png) | ![](./screenshot4.png) |
| ![](./screenshot5.png) | ![](./screenshot6.png) |
