[← Homepage](../README.md)

# Enhancements for AlternativeTo

Filtering enhancements for AlternativeTo.net

| [Install](../../../../raw/branch/master/alternativetoEnhanced/main.user.js) |
|-----------------------------------------------------------------------------|

| ![](./screenshot1.png) | ![](./screenshot2.png) |
|------------------------|------------------------|

## Changelog

* `0.1.0` (2022-12-20) • [Initial release](https://git.kaki87.net/KaKi87/userscripts/src/commit/2b77b50b177e857c10fd6478b9cc95b9adf68dd1/alternativetoEnhanced)