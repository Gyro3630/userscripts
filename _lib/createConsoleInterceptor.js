var createConsoleInterceptor = (
    methods,
    onConsole
) => Object.entries(methods).reduce(
    (
        interceptedMethods,
        [methodName, method]
    ) => ({
        ...interceptedMethods,
        [methodName]: (...args) => {
            method(...args);
            onConsole[methodName](args);
        }
    }),
    {}
);