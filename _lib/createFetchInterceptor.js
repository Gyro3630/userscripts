var createFetchResponseInterceptor = ({
    fetch,
    onRequest,
    onResponse
}) => async (resource, options) => {
    const
        url = resource.url || resource,
        {
            resource: modifiedResource,
            config: modifiedOptions
        } = await onRequest?.({
            url,
            resource,
            options
        }) || {},
        response = await fetch(
            modifiedResource || resource,
            modifiedOptions || options
        ),
        modifiedUrl = modifiedResource?.url || modifiedResource,
        modifiedResponse = await onResponse({
            url: modifiedUrl || url,
            resource: modifiedResource || resource,
            options: modifiedOptions || options,
            response: response.clone()
        });
    return modifiedResponse || response;
};