var createXhrInterceptor = (({
    xhrOpen,
    onResponse
}) => function(method, url, async, user, password){
    this.addEventListener(
        'load',
        () => onResponse(this)
    );
    return xhrOpen.apply(this, arguments);
});