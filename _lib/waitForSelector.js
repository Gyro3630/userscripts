/**
 * @callback waitForSelectorCallback
 * @param {HTMLElement} element
 * @returns {boolean} isStop
 */
/**
 * Returns element(s) matching the specified selector as soon as found.
 * @param {string} selector
 * @param {waitForSelectorCallback} [callback]
 * @returns {Promise<HTMLElement|void>} element
 */
var waitForSelector = (
    selector,
    callback
) => new Promise(resolve => {
    let observer;
    const
        results = [],
        observerCallback = () => {
            const elements = document.querySelectorAll(selector);
            for(const element of elements){
                if(!results.includes(element)){
                    results.push(element);
                    if(callback){
                        if(callback(element)){
                            resolve(element);
                            return observer.disconnect();
                        }
                    }
                    else {
                        resolve(element);
                        return observer.disconnect();
                    }
                }
            }
        };
    observer = new MutationObserver(observerCallback);
    observerCallback();
    observer.observe(
        document.documentElement,
        {
            childList: true,
            subtree: true
        }
    );
});