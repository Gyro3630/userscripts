[← Homepage](../README.md)

# XDA Forums Threads Meta

Add created & updated metadata to XDA forum threads.

| [Install](../../../../raw/branch/master/XDAForumsThreadsMeta/main.user.js) |
|----------------------------------------------------------------------------|

## DEPRECATION NOTICE

[I firstly created this script in order to get the updated date of threads *(i.e. custom ROMs)* in a topic *(i.e. a device)* directly from the topics list.](https://forum.xda-developers.com/general/about-xda/feature-request-sort-topics-time-edited-t3943066)

It seems, though, that edited date is no longer available on threads.

Therefore, this script has outlived its usefulness and will no longer receive updates unless the edited date information comes back.

---

## Changelog

* `1.0.0` (2019-06-26) • [Initial release](https://git.kaki87.net/KaKi87/userscripts/src/commit/3330f9a72505d8d9b128ca6c990b20cc1848f524/XDAForumsThreadsMeta)