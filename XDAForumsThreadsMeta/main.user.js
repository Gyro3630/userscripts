// ==UserScript==
// @match       https://forum.xda-developers.com/*
// @name        XDA Forums - Threads meta
// @description Add created & updated metadata to XDA forum threads
// @grant       none
// @version     1.0.0
// @author      KaKi87
// @license     MIT
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/XDAForumsThreadsMeta
// ==/UserScript==

window.addEventListener('DOMContentLoaded', () => {
    if(!document.querySelector('.thread-listing')) return;
    const getThreadMeta = link => new Promise(resolve => $.get(link, data => {
        const zip = (keys, values) => Object.assign(...keys.map((k, i) => ({ [k]: values[i] }))); // Source : https://stackoverflow.com/a/47517569
        const thread = new DOMParser().parseFromString(data, 'text/html');
        resolve({
            created: zip(['date', 'time'],
                thread.querySelector('#thread-header-meta')
                      .textContent.trim().split('\n')[0].split(' on ')[1].split(', ')),
            ...(thread.querySelector('.postbit-edited') ? {
                edited: zip(['author', 'date', 'time'], thread.querySelector('.postbit-edited').textContent.match(/Last edited by (.+); (.+) at (.+)\./).slice(1))
            } : {})
        });
    }));
    (async() => {
        const threads = [...document.querySelectorAll('.thread-row')].map(el => ({
            link: el.querySelector('.threadTitle').href,
            author: el.querySelector('div.smallfont a').textContent,
            subtitle: el.querySelector('div.smallfont')
        }));
        for(let i = 0; i < threads.length; i++){
            const
                thread = threads[i],
                meta = await getThreadMeta(thread.link);
            thread.subtitle.innerHTML += `, created on ${meta.created.date} ${meta.created.time}${meta.edited ? `, edited on ${meta.edited.date} ${meta.edited.time} ${(thread.author !== meta.edited.author) ? `by ${meta.edited.author}` : ''}` : ''}`;
        }
    })();
});