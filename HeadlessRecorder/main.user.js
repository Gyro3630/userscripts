// ==UserScript==
// @match       *://*/*
// @name        Headless recorder
// @description Record browser actions & generate headless browser code
// @grant       GM.getValue
// @grant       GM.setValue
// @grant       GM.registerMenuCommand
// @grant       GM.xmlHttpRequest
// @version     1.0.0
// @author      KaKi87
// @license     GPL-3.0-or-later
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/HeadlessRecorder
// ==/UserScript==

const
    devControlsUrl = 'http://localhost:3250/controls.html',
    prodControlsUrl = 'https://userscripts.kaki87.net/HeadlessRecorder/controls.html',
    getValue = async key => JSON.parse(await GM.getValue(key) || '{}'),
    setValue = async (key, value) => await GM.setValue(key, JSON.stringify(value)),
    getElementSelector = element => {
        const
            path = [],
            path2Selector = path => path.join(' > '),
            isUnique = (selector, parent) => (parent || document).querySelectorAll(selector).length === 1;
        let currentElement;
        while(!path.length || !isUnique(path2Selector(path))){
            currentElement = currentElement ? currentElement.parentElement : element;
            const
                uniqueClassName = [...currentElement.classList].find(className => isUnique(path2Selector([`.${className}`, ...path]))),
                { parentElement } = currentElement ,
                tagName = currentElement.tagName.toLowerCase();
            if(currentElement.id)
                path.unshift(`#${currentElement.id}`);
            else if(uniqueClassName)
                path.unshift(`.${uniqueClassName}`);
            else if(parentElement){
                if(isUnique(tagName, parentElement))
                    path.unshift(tagName);
                else
                    path.unshift(`${tagName}:nth-child(${[...parentElement.children].indexOf(currentElement) + 1})`);
            }
            else
                path.unshift(tagName);
        }
        return path2Selector(path);
    },
    formats = ['javascript', 'cypress'],
    json2code = ({
        selector,
        action,
        key,
        value
    }, format) => ({
        javascript: ({
            navigate: `window.location.href = '${value}'`,
            click: `document.querySelector('${selector}').click()`,
            type: `document.querySelector('${selector}').${key} = '${value}'`,
            expect: ({
                'textContent': `document.querySelector('${selector}').textContent.includes('${value}')`
            })[key] || `document.querySelector('${selector}').getAttribute('${key}').includes('${value}')`
        })[action],
        cypress: ({
            navigate: `cy.visit('${value}')`,
            click: `cy.get('${selector}').click()`,
            type: `cy.get('${selector}').type('${value}')`,
            expect: ({
                'textContent': `cy.get('${selector}').contains('${value}')`
            })[key] || `cy.get('${selector}').should('have.attr', '${key}', '${value}')`
        })[action]
    }[format]);

if([devControlsUrl, prodControlsUrl].includes(window.location.href)){
    Object.assign(unsafeWindow, {
        getValue,
        setValue,
        formats,
        json2code
    });
}
else {
    GM.registerMenuCommand('Open', async () => {
        const status = await getValue('status');

        if(typeof status.isDev === 'undefined'){
            status.isDev = await new Promise(resolve => {
                GM.xmlHttpRequest({
                    url: devControlsUrl,
                    onload: () => resolve(true),
                    onerror: () => resolve(false)
                });
            });
            await setValue('status', status);
        }

        window.open(
            status.isDev ? devControlsUrl : prodControlsUrl,
            '_blank',
            'location=no,top=0,left=0,width=640,height=360'
        );
    });

    ['click', 'keyup'].forEach(eventName => document.addEventListener(eventName, async event => {
        const
            history = await getValue('history'),
            {
                mode,
                navigate,
                wait
            } = await getValue('status'),
            selector = getElementSelector(event.target);
        if(!history.items) history.items = [];
        if(mode && mode !== 'disabled' && navigate){
            const
                url = window.location.href,
                { value: lastUrl } = [...history.items].reverse().find(item => item.action === 'navigate') || {};
            if(!lastUrl || lastUrl !== url)
                history.items.push({ action: 'navigate', value: url });
        }
        switch(eventName){
            case 'click': {
                switch(mode){
                    case 'default': {
                        if(wait) history.items.push({ selector, action: 'wait' });
                        history.items.push({ selector, action: 'click' });
                        break;
                    }
                    case 'expect': {
                        event.preventDefault();
                        event.stopImmediatePropagation();
                        if(wait) history.items.push({ selector, action: 'wait' });
                        const key = {
                            'INPUT': 'value',
                            'A': 'href',
                            'IMG': 'src'
                        }[event.target.tagName] || 'textContent';
                        history.items.push({
                            selector,
                            action: 'expect',
                            key,
                            value: (event.target[key] || event.target.getAttribute(key)).trim()
                        });
                        break;
                    }
                }
                break;
            }
            case 'keyup': {
                const
                    lastItem = (history.items || []).slice(-1)[0],
                    { target } = event,
                    key = target.hasAttribute('contenteditable') ? 'textContent' : 'value',
                    { [key]: value } = target;
                switch(mode){
                    case 'default': {
                        if(lastItem && lastItem.selector === selector && lastItem.action === 'type')
                            lastItem.value = value;
                        else {
                            if(wait) history.items.push({ selector, action: 'wait' });
                            history.items.push({
                                selector,
                                action: 'type',
                                key,
                                value
                            });
                        }
                        break;
                    }
                }
                break;
            }
        }
        await setValue('history', history);
    }));
}