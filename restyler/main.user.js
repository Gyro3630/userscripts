// ==UserScript==
// @match         *://*/*
// @name          Restyler
// @description   De-style & re-style any website with any stylesheet.
// @grant         GM.registerMenuCommand
// @grant         GM.addElement
// @version       0.1.0
// @author        KaKi87
// @license       MIT
// @namespace     https://git.kaki87.net/KaKi87/userscripts/src/branch/master/restyler
// ==/UserScript==

let
    areAllEventsRemoved = false,
    isMetaViewportAdded = false;

const
    removeAllStyles = () => {
        document.querySelectorAll('style, link[rel=stylesheet]').forEach(el => el.remove());
        document.querySelectorAll('[style]').forEach(el => el.removeAttribute('style'));
    },
    removeAllEvents = () => {
        if(!areAllEventsRemoved){
            document.head.appendChild(Object.assign(document.createElement('meta'), { 'name': 'darkreader-lock' }));
            document.documentElement.innerHTML = document.documentElement.innerHTML;
            areAllEventsRemoved = true;
        }
    },
    addMetaViewport = () => {
        if(!isMetaViewportAdded){
            document.head.appendChild(Object.assign(
                document.createElement('meta'),
                {
                    'name': 'viewport',
                    'content': 'width=device-width, initial-scale=1'
                })
            );
            isMetaViewportAdded = true;
        }
    },
    stylesheets = [
        {
            name: 'Water.css',
            urls: [
                'https://cdn.jsdelivr.net/npm/water.css/out/water.css'
            ]
        },
        {
            name: 'Classless.css',
            urls: [
                'https://cdn.jsdelivr.net/gh/emareg/classlesscss/docs/classless.css',
                'https://classless.de/addons/themes.css'
            ],
            onLoad: () => {
                if(window.matchMedia('(prefers-color-scheme: dark)').matches)
                    document.documentElement.setAttribute('data-theme', 'dark');
            }
        },
        {
            name: 'Pico.css',
            urls: [
                'https://cdn.jsdelivr.net/npm/@picocss/pico/css/pico.min.css'
            ]
        }
    ];

for(const stylesheet of stylesheets){
    GM.registerMenuCommand(
        `Re-style with ${stylesheet.name}`,
        async () => {
            removeAllStyles();
            removeAllEvents();
            addMetaViewport();
            for(const url of stylesheet.urls){
                await new Promise(resolve => {
                    const styleElement = GM.addElement(
                        'link',
                        {
                            'rel': 'stylesheet',
                            'href': url
                        }
                    );
                    styleElement.addEventListener('load', resolve);
                    styleElement.addEventListener('error', resolve);
                });
            }
            stylesheet.onLoad?.();
        }
    );
}

GM.registerMenuCommand(
    'De-style',
    removeAllStyles
);

GM.registerMenuCommand(
    'Remove all events',
    removeAllEvents
);

GM.registerMenuCommand(
    'Add meta viewport',
    addMetaViewport
);