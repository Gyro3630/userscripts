[← Homepage](../README.md)

# Free NPM Explore

Explore NPM packages for free.

| [Install](../../../../raw/branch/master/FreeNpmExplore/main.user.js) |
|----------------------------------------------------------------------|

## DEPRECATION NOTICE

This project is now deprecated since [that feature is no longer paid](https://old.reddit.com/r/node/comments/zvuhg8/the_npm_code_formerly_explore_tab_is_free_for/).

---

![](./screenshot.png)

## Changelog

* `0.1.0` (2021-11-29) • [Initial release](https://git.kaki87.net/KaKi87/userscripts/src/commit/c83e11ab0fe04073b7d8e4d03ced0c34f2e8410b/FreeNpmExplore)
* `0.1.1` (2022-11-26) • [Fix unhandled 'activeTab' query parameter](https://git.kaki87.net/KaKi87/userscripts/src/commit/6d316d614297e02e6ab047fd030499cc35ef8d94/FreeNpmExplore)