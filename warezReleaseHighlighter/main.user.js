// ==UserScript==
// @match       https://*.yggtorrent.*/*
// @match       https://ygg.*/*
// @name        Warez release highlighter
// @description Syntax highlighting for warez release names
// @version     0.1.0
// @author      KaKi87
// @license     GPL-3.0-or-later
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/warezReleaseHighlighter
// ==/UserScript==

(async () => {
    const { default: parseReleaseName } = await import(`http://localhost:3000/index.js?${Date.now()}`);
    for(const torrentElement of document.querySelectorAll('#torrent_name')){
        const release = parseReleaseName(torrentElement.textContent);
        torrentElement.innerHTML = '';
        for(const chunk of release.chunks){
            const chunkElement = document.createElement('span');
            chunkElement.classList.add('chunk');
            chunkElement.classList.toggle('chunk--separator', chunk.isSeparator && !chunk.property);
            if(chunk.property)
                chunkElement.classList.add(`chunk--${chunk.property}`);
            chunkElement.textContent = chunk.value;
            torrentElement.appendChild(chunkElement);
        }
    }
})().catch(console.error);

const styles = new CSSStyleSheet();
// language=CSS
styles.replaceSync(`
    .chunk--separator,
    .chunk--ignored {
        opacity: 0.5;
    }
    .chunk--episode,
    .chunk--seasonNumber {
        color: #E6194B;
    }
    .chunk--date,
    .chunk--years {
        color: #3CB44B;
    }
    .chunk--languages { color: #FFE119; }
    .chunk--resolution { color: #0082C8; }
    .chunk--source { color: #FABEBE; }
    .chunk--codecs,
    .chunk--dolbySpecs,
    .chunk--dtsSpecs,
    .chunk--encoding,
    .chunk--surround,
    .chunk--isHdr {
        color: #46F0F0;
    }
    .chunk--isDolbyVision { color: #F032E6; }
    .chunk--bitrate,
    .chunk--bitDepth,
    .chunk--sampleRate,
    .chunk--frequency {
        color: #008080;
    }
    .chunk--isRemux { color: #F58231; }
    .chunk--format { color: #E6194B; }
`);
document.adoptedStyleSheets = [...document.adoptedStyleSheets, styles];