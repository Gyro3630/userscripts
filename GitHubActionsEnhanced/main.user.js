// ==UserScript==
// @match       https://github.com/*/*/actions
// @name        Enhancements for GitHub Actions
// @description Enhanced GitHub Actions with notification updates & progress bar
// @grant       GM_notification
// @version     1.0.0
// @author      KaKi87
// @license     GPL-3.0-or-later
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/GitHubActionsEnhanced
// ==/UserScript==

const
    _username = document.querySelector('img[alt^="@"]').alt.slice(1),
    _lastActionStatus = {},
    getActions = () => [...document.querySelectorAll('[id^="check_suite"]')].map(el => {
        let status;
        const
            svgClassList = el.querySelector('svg').classList,
            titleLink = el.querySelector('a'),
            startTimestamp = new Date(el.querySelector('time-ago').getAttribute('datetime')).valueOf(),
            [seconds, minutes, hours] = ((el.querySelector('.issue-keyword') || {}).textContent || '').trim().split(' ').reverse().map(_ => parseInt(_));
        if(svgClassList.contains('octicon-check-circle-fill'))
            status = 'success';
        if(svgClassList.contains('octicon-x-circle-fill'))
            status = 'failure';
        if(svgClassList.contains('octicon-stop'))
            status = 'aborted';
        if(svgClassList.contains('anim-rotate'))
            status = 'in progress';
        if(svgClassList.contains('octicon-dot-fill'))
            status = 'idle';
        return {
            id: parseInt(titleLink.href.split('/').slice(-1)[0]),
            title: titleLink.textContent,
            status,
            author: el.querySelector('a[data-hovercard-type]').textContent,
            branch: (el.querySelector('.branch-name') || {}).textContent,
            startTimestamp,
            duration: seconds
                ? seconds + (minutes || 0) * 60 + (hours || 0) * 3600
                : (Date.now() - startTimestamp) / 1000
        };
    }).map((action, undefined, actions) => {
        const
            branchActions = actions.filter(_action => _action.branch === action.branch),
            averageBranchActionDuration = branchActions.map(_action => _action.duration).reduce((a, b) => a + b, 0) / branchActions.length,
            lastBranchActionDurations = branchActions.filter(_action => _action.startTimestamp < action.startTimestamp).map(_action => _action.duration),
            lastBranchActionDuration = lastBranchActionDurations[0],
            lastBranchActionDurationAboveAverage = lastBranchActionDurations.find(duration => duration > averageBranchActionDuration),
            maxActionDuration = Math.max(...actions.map(item => item.duration));
        return {
          ...action,
          progressPercent: action.duration / (lastBranchActionDurationAboveAverage || lastBranchActionDuration || maxActionDuration) * 100
        };
    });

getActions().forEach(action => _lastActionStatus[action.id] = action.status);

setInterval(() => getActions().forEach((action, undefined, actions) => {
    if(_lastActionStatus[action.id] !== action.status && (action.author === _username || action.branch === 'master')){
        const { length: branchActionsCount } = actions.filter(_action => ['in progress', 'idle'].includes(_action.status) && _action.branch === action.branch);
        GM_notification({
            ...branchActionsCount > 1 ? {
                title: '[WARNING] GitHub Actions',
                text: `${branchActionsCount} concurrent actions on branch ${action.branch}`
            } : {
                title: `GitHub Actions update (${action.id})`,
                text: `${action.title}\nStatus : ${action.status} ${action.progressPercent ? `(${action.progressPercent.toFixed(2)}%)` : ''}`
            },
            image: 'https://git.kaki87.net/KaKi87/userscripts/raw/branch/master/GitHubActionsNotifications/assets/logo_github.png'
        });
    }
    _lastActionStatus[action.id] = action.status;
}), 500);

{
    const
        progressPercentStyle = document.createElement('style'),
        progressPercentDiv = document.createElement('div');
    progressPercentStyle.appendChild(document.createTextNode(`
        .gae_progress-percent {
            position: fixed;
            bottom: 0;
            left: 0;
            height: 1rem;
            background-color: var(--color-success-emphasis);
            box-shadow: 0px -1px 4px var(--color-success-emphasis);
            transition: transform 0.5s;
        }
        .gae_progress-percent:before {
            content: '';
            position: absolute;
            bottom: 1rem;
            left: 0;
            width: 100%;
            height: 1rem;
        }
        .gae_progress-percent:hover, .gae_progress-percent--hidden {
            transform: translateY(100%);
        }
    `));
    progressPercentStyle.classList.add('gae_progress-percent-style');
    progressPercentDiv.classList.add('gae_progress-percent');
    document.head.appendChild(progressPercentStyle);
    document.body.appendChild(progressPercentDiv);
    setInterval(() => {
        const [ { status, progressPercent } ] = getActions();
        progressPercentDiv.style.width = `${progressPercent || 0}vw`;
        progressPercentDiv.classList.toggle('gae_progress-percent--hidden', status !== 'in progress' || progressPercent >= 100);
    }, 500);
}

unsafeWindow._getActions = getActions;