[← Homepage](../README.md)

# Enhancements for Forgejo

| [Install](../../../../raw/branch/master/enhancementsForForgejo/main.user.js) |
|------------------------------------------------------------------------------|

## Features

- Fix alphabetical files ordering ([`forgejo/forgejo@codeberg.org#317`](https://codeberg.org/forgejo/forgejo/issues/317))
- Fix comment buttons alignment ([`forgejo/forgejo@codeberg.org#560`](https://codeberg.org/forgejo/forgejo/issues/560))
- Sticky navbar ([`forgejo/forgejo@codeberg.org#1318`](https://codeberg.org/forgejo/forgejo/issues/1318))
- Scrollable code view
- Fix issues tab consistency ([`forgejo/forgejo@codeberg.org#365`](https://codeberg.org/forgejo/forgejo/issues/365))

---

| ![](./screenshot1.png) | ![](./screenshot2.png) |
|------------------------|------------------------|
| ![](./screenshot3.png) | ![](./screenshot4.png) |