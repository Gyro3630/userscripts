// ==UserScript==
// @match       https://imgbb.com
// @match       https://*.imgbb.com/
// @name        ImgBB direct link
// @description Automatically redirect to direct link after ImgBB upload
// @version     0.1.0
// @author      KaKi87
// @license     GPL-3.0-or-later
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/imgbbDirectLink
// ==/UserScript==

new MutationObserver(() => window.location.replace(new DOMParser().parseFromString(document.querySelector('#uploaded-embed-code-1').value, 'text/html').querySelector('img').getAttribute('src'))).observe(document.body, { childList: true, subtree: true });