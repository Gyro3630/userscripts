[← Homepage](../README.md)

# ImgBB direct link

Automatically redirect to direct link after ImgBB upload.

| [Install](../../../../raw/branch/master/imgbbDirectLink/main.user.js) |
|-----------------------------------------------------------------------|