# Userscripts

[![](https://shields.kaki87.net/discord/739600823415472128)](https://discord.gg/gSJ3dRM)

A userscript[^1] is a JavaScript[^2] file that can modify (usually improve) your browsing experience similarly to browser extensions[^3] but with limited capabilities and less development effort.

Though many browser extensions exist for managing userscripts, Violentmonkey[^4] is recommended for all platforms.

## Featured userscripts

| [Cloudflare detector for Google search](./GoogleSearchCloudFlareDetector/README.md) | [Dark Reader dynamic blacklist](./darkReaderDynamicBlacklist/README.md) |     [Enhancements for Elk Zone](./enhancementsForElkZone/README.md)     |
|:-----------------------------------------------------------------------------------:|:-----------------------------------------------------------------------:|:-----------------------------------------------------------------------:|
|        Tag Google search results pointing to Cloudflare-protected websites.         |        Dynamically disable Dark Reader on already dark websites.        |    Add useful features & UI/UX fixes to the Elk client for Mastodon.    |
|         **[Enhancements for Forgejo](./enhancementsForForgejo/README.md)**          |       **[Fediverse redirector](./fediverseRedirector/README.md)**       | **[Responsive design for Reddit Old](./redditOldResponsive/README.md)** |
|           Add useful features & UI/UX fixes for the Forgejo git platform.           |            Redirect any Fediverse app to your favourite one.            |               Mobile-friendly design for old.reddit.com.                |

## All userscripts

- [Bing images exact size](./bingImagesExactSize/README.md)
- [Cloudflare detector for Brave search](./braveSearchCloudflareDetector/README.md)
- [Cloudflare detector for Google search](./GoogleSearchCloudFlareDetector/README.md)
- [Cloudflare detector for Qwant search](./qwantCloudflareDetector/README.md)
- [Dark Reader dynamic blacklist](./darkReaderDynamicBlacklist/README.md)
- [Downloader for Cafeyn](./CafeynDL/README.md)
- [Enhancements for AlloVoisins](./enhancementsForAllovoisins/README.md) *(FR)*
- [Enhancements for AlternativeTo](./alternativetoEnhanced/README.md)
- [Enhancements for Dealabs](./dealabsEnhanced/README.md)
- [Enhancements for Elk Zone](./enhancementsForElkZone/README.md)
- [Enhancements for Forgejo](./enhancementsForForgejo/README.md)
- [Enhancements for GitHub Actions](./GitHubActionsEnhanced/README.md)
- [Enhancements for JSBin](./jsbinEnhanced/README.md)
- [Fediverse redirector](./fediverseRedirector/README.md)
- [GitHub Raw & IO to COM](./githubRawAndIoToCom/README.md)
- [ImgBB direct link](./imgbbDirectLink/README.md)
- [ProtonMail mobile fixes](protonmailMobileFixes/README.md)
- [ProtonMail without annoyances](./protonmailWithoutAnnoyances/README.md)
- [Reddit Old Better Editor](./redditOldBetterEditor/README.md)
- [Responsive design for Reddit Old](./redditOldResponsive/README.md)
- [YGGTorrent Infinite Session](./YGGTorrentInfiniteSession/README.md) *(FR)*
- [YouTube Click To Play](./youtubeClickToPlay/README.md) *(from [knoajp](https://github.com/knoajp))*

## Deprecated userscripts

- [AlternativeTo free/freemium differentiator](./AlternativeToFreeFreemiumDifferentiator/README.md)
- [Enhancements for Reddit Chat](./redditOldBetterChat/README.md)
- [Free NPM Explore](./FreeNpmExplore/README.md)
- [GitHub Actions notifications](./GitHubActionsNotifications/README.md)
- [LeBonCoin phone bypasser](./leboncoinPhoneBypasser/README.md)
- [Mastodon redirector](./mastodonRedirector/README.md)
- [MastoVue embedded](./mastovueEmbedded/README.md)
- [RARBG threat defence bypasser](./rarbgThreatDefenceBypasser/README.md)
- [Reddit Force Local Search](./RedditForceLocalSearch/README.md)
- [Splix.IO assistant](./SplixAssistant/README.md)
- [XDA Forums Threads Meta](./XDAForumsThreadsMeta/README.md)

## [Featured third-party userscripts](https://git.kaki87.net/KaKi87/config/src/branch/master/brave-browser/jinjaccalgkegednnccohejagnlnfdag)

[^1]: [Userscript - Wikipedia](https://en.wikipedia.org/wiki/Userscript)
[^2]: [JavaScript - Wikipedia](https://en.wikipedia.org/wiki/JavaScript)
[^3]: [Browser extension - Wikipedia](https://en.wikipedia.org/wiki/Browser_extension)
[^4]: [Violentmonkey - Official website](https://violentmonkey.github.io/)