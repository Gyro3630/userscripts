// ==UserScript==
// @match       https://*/*
// @name        MastoVue embedded
// @description MastoVue embedded into Mastodon
// @grant       GM.setClipboard
// @grant       GM.addStyle
// @version     0.3.0
// @author      KaKi87
// @license     MIT
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/mastovueEmbedded
// ==/UserScript==

const
    APP_ID = 'mastovueEmbedded',
    APP_ICON = 'fa-wrench',
    APP_NAME = 'MastoVue',
    MASTOVUE_URL = 'https://mastovue.glitch.me';

if(document.querySelector('#mastodon')){
    new MutationObserver(() => {
        document.querySelectorAll('[href="/web/lists"], [href="/lists"]').forEach(element => {
            if(!element.parentElement.querySelector(`.__${APP_ID}__navItem`)){
                element.parentElement.style.height = 'auto';
                const
                    navItemElement = element.cloneNode(true),
                    [
                        navItemIconElement,
                        navItemTextElement
                    ] = navItemElement.childNodes;
                navItemElement.classList.add(`__${APP_ID}__navItem`);
                navItemElement.classList.toggle('active', false);
                navItemElement.setAttribute('href', `#__${APP_ID}`);
                navItemElement.setAttribute('title', APP_NAME);
                navItemIconElement.classList.remove('fa-list-ul');
                navItemIconElement.classList.add(APP_ICON);
                navItemTextElement.textContent = APP_NAME;
                element.insertAdjacentElement('afterend', navItemElement);
            }
        });
    }).observe(document.body, { subtree: true, attributes: true });
    const containerElement = document.createElement('div');
    containerElement.classList.add(`__${APP_ID}__container`);
    {
        const
            loaderElement = document.createElement('div'),
            iframeElement = document.createElement('iframe');
        loaderElement.classList.add(`__${APP_ID}__loader`);
        loaderElement.innerHTML = `<i class="fa fa-spinner fa-spin"></i>`;
        containerElement.appendChild(loaderElement);
        iframeElement.classList.add(`__${APP_ID}__iframe`);
        iframeElement.setAttribute('src', MASTOVUE_URL);
        iframeElement.addEventListener('load', () => {
            const postMessageInterval = setInterval(
                () => iframeElement.contentWindow.postMessage(
                    { isMastodon: true },
                    MASTOVUE_URL
                ),
                500
            );
            window.addEventListener('message', event => {
                if(event.origin !== MASTOVUE_URL) return;
                const {
                    isMastodonAck,
                    mastodonSearchUrl
                } = event.data;
                if(isMastodonAck){
                    clearInterval(postMessageInterval);
                    containerElement.classList.add(`__${APP_ID}__container--loaded`);
                }
                if(mastodonSearchUrl){
                    GM.setClipboard(mastodonSearchUrl);
                    const notificationElement = document.createElement('div');
                    notificationElement.classList.add(`__${APP_ID}__notification`);
                    notificationElement.innerHTML = `<i class="fa fa-check"></i>&nbsp;Link copied`;
                    document.body.appendChild(notificationElement);
                    setTimeout(() => notificationElement.remove(), 3000);
                }
            });
        });
        containerElement.appendChild(iframeElement);
    }
    {
        let oldHash, isAdvanced;
        (function _(){
            const hash = window.location.hash;
            if(oldHash !== hash){
                oldHash = hash;
                if(hash === `#__${APP_ID}`){
                    const
                        simpleMainElement = document.querySelector('.columns-area__panels__main'),
                        advancedMainElement = document.querySelector('.columns-area:not(.columns-area--mobile)'),
                        mainElement = simpleMainElement || advancedMainElement;
                    if(typeof isAdvanced === 'undefined')
                        isAdvanced = !simpleMainElement && !!advancedMainElement;
                    if(mainElement){
                        if(!mainElement.contains(containerElement)){
                            containerElement.classList.toggle('column', isAdvanced);
                            mainElement.appendChild(containerElement);
                        }
                        containerElement.classList.add(`__${APP_ID}__container--visible`);
                        document.querySelectorAll(`.__${APP_ID}__navItem`).forEach(element => element.classList.add('active'));
                    }
                }
                else if(!isAdvanced){
                    containerElement.classList.remove(`__${APP_ID}__container--visible`);
                    document.querySelectorAll(`.__${APP_ID}__navItem`).forEach(element => element.classList.remove('active'));
                }
            }
            setTimeout(_, 100);
        })();
    }
    GM.addStyle(`
        :root {
            --background-color: ${window.getComputedStyle(document.body).getPropertyValue('background-color')}
        }
        .columns-area__panels__main {
            position: relative;
        }
        .__${APP_ID}__container:not(.__${APP_ID}__container--visible),
        .__${APP_ID}__container--loaded .__${APP_ID}__loader,
        .__${APP_ID}__container:not(.__${APP_ID}__container--loaded) .__${APP_ID}__iframe {
            display: none;
        }
        .__${APP_ID}__container:not(.column) {
            z-index: 1;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: var(--background-color);
        }
        .__${APP_ID}__container.column {
            order: 9999;
        }
        .__${APP_ID}__loader {
            width: 100%;
            height: 100vh;
            display: flex;
            align-items: center;
            justify-content: center;
            font-size: 3rem;
        }
        .__${APP_ID}__iframe {
            width: 100%;
            height: 100%;
        }
        .__${APP_ID}__notification {
            z-index: 1;
            position: absolute;
            bottom: 0;
            left: 50%;
            transform: translateX(-50%);
            margin-bottom: 1rem;
            background-color: var(--background-color);
            color: white;
            padding: 1rem;
        }
    `);
}

if(window.location.origin === MASTOVUE_URL){
    let
        mastodonWindow,
        mastodonUrl;
    window.addEventListener('message', event => {
        if(event.data.isMastodon){
            ({
                source: mastodonWindow,
                origin: mastodonUrl
            } = event);
            mastodonWindow.postMessage({ isMastodonAck: true }, mastodonUrl);
        }
    });
    document.addEventListener('click', event => {
        const url = event.target.closest('a')?.href;
        if(url){
            const urlPath = new URL(url).pathname;
            if(
                urlPath.startsWith('/@')
                ||
                urlPath.startsWith('/users/')
            ){
                event.preventDefault();
                mastodonWindow.postMessage({ mastodonSearchUrl: url }, mastodonUrl);
            }
        }
    });
    {
        const styleElement = document.createElement('style');
        styleElement.appendChild(document.createTextNode(`
            :root {
                color-scheme: dark;
            }
            html {
                filter: invert(1) hue-rotate(180deg) contrast(.7);
            }
            html * {
                background: #fefefe;
            }
            img {
                filter: invert(1) hue-rotate(180deg) contrast(1);
            }
            .glitchButton {
                display: none;
            }
            .section,
            .button {
                padding-left: 0.5rem;
                padding-right: 0.5rem;
            }
            .button,
            .input,
            .icon {
                font-size: 1rem !important;
            }
            .control.is-large {
                flex: 1;
            }
        `));
        document.head.appendChild(styleElement);
    }
}