# Contributing guidelines

Understand the userscript in order to contribute to it.

## Blocks

The userscript's core is divided into blocks.

### Domain list

The domain list block is the content of `switch(window.location.hostname)`, which allows disabling Dark Reader on specified domain names without delay.

It is divided into sections according to [use cases](./README.md#use-cases).

In each section, domains are sorted by :

1. Second-level domain
2. Lower-level domains
3. Top-level domain

This allows better grouping of related domains, e.g. :

- `alice.me`
- `example.com`
- `www.example.com`
- `example.io`
- `app.example.io`
- `frank.me`

Unlike simple alphabetic sorting, e.g. :

- `alice.me`
- `app.example.io`
- `example.com`
- `example.io`
- `frank.me`
- `www.example.com`

For websites featuring a system-enabled dark theme, adding one is as simple as inserting `case 'example.com':`.

*(You may stop reading at this point if this is the only use case you're interested in).*

For websites featuring a user-enabled dark theme, it should be automatically enabled, which usually involves one of the following actions :
- setting a [`window.localStorage`](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage) value ;
- setting a [`document.cookie`](https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie) value ;
- [`click`](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/click)-ing an element.

For websites featuring multiple themes, one could be automatically enabled the same way, and the [*Open theme switcher* feature](README.md#multiple-themes) should be implemented using the [`setThemeSwitcherOpener`](#setthemeswitcheropener) method, which usually involves one of the following actions :
- navigating to a local path using [`window.location.assign`](https://developer.mozilla.org/en-US/docs/Web/API/Location/assign) ;
- waiting for & clicking elements.

In this block, each [`disableDarkReader`](#disabledarkreader) call should be preceded by a [`preventObserver`](#preventobserver) call.

### Observer

The observer block is the content of `observerCallback`, which is a [`MutationObserver`](https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver) callback, fired whenever the page content changes, as well as when the [`DOMContentLoaded`](https://developer.mozilla.org/en-US/docs/Web/API/Window/DOMContentLoaded_event) event is fired.

It allows detecting a website manually, or using one of the following presets :
- [`<meta property="og:*">`](https://ogp.me/) ;
- [`<noscript>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/noscript) ;
- [`<link rel="manifest">`](https://developer.mozilla.org/en-US/docs/Web/Manifest) ;
- [`<meta name="generator">`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta/name#standard_metadata_names_defined_in_the_html_specification).

In order to facilitate strategizing, all those values are logged when detected.

The first condition allowing a website to be detected should be preceded by a Markdown hyperlink comment pointing to its homepage and followed by a [`stopObserver`](#stopobserver) call. Blocking asynchronous code may be used from this point but not before.

## Methods

Methods help implementing the userscript's core.

### `preventObserver`

Prevent the observer from starting.

Must be called after detecting a website before reaching the [observer block](#observer), e.g. before each [`disableDarkReader`](#disabledarkreader) call in the [domain list block](#domain-list).

### `stopObserver`

Stop the observer.

Must be called after detecting a website inside or below the [observer block](#observer).

### `disableDarkReader`

Append an [`<meta name="darkreader-lock">`](https://github.com/darkreader/darkreader/pull/9181) element to disable Dark Reader.

Must be called after ensuring that the detected website is featuring a dark mode.

### `setThemeSwitcherOpener`

Implement the [*Open theme switcher* feature](README.md#multiple-themes) by passing a callback containing the necessary steps.

### Other methods

- JSDocumented in userscript : `onDom`, `onComplete` and `hasAnyClass` ;
- JSDocumented as dependency : [`waitForSelector`](../_lib/waitForSelector.js).