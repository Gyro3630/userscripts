[← Homepage](../README.md)

# GitHub RAW & IO to COM

Redirect `raw.githubusercontent.com/{username}/{repo}/*` & `{username}.github.io` to `github.com/{username}/{repo}` on demand.

| [Install](../../../../raw/branch/master/githubRawAndIoToCom/main.user.js) |
|---------------------------------------------------------------------------|

## Changelog

* `0.1.0` (2022-04-28) • [Initial release](https://git.kaki87.net/KaKi87/userscripts/src/commit/9dcedd285752555ac37b6bc53dda3ea0f0f92fd8/githubRawAndIoToCom)
* `0.1.1` (2023-01-31) • [Fix 'github.io' support on custom repo name](https://git.kaki87.net/KaKi87/userscripts/src/commit/6e41c4eb854d3e2a9fe74b9aece86d3ec3902dec/githubRawAndIoToCom)
* `0.2.0` (2023-01-31) • [[Rewrite] Fix "domain is repo" & "top directory is repo" case handling](https://git.kaki87.net/KaKi87/userscripts/src/commit/bead059e5079c7846529d718e86079c3aff94bd4/githubRawAndIoToCom)