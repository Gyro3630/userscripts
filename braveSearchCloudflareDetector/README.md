[← Homepage](../README.md)

# Cloudflare detector for Brave search *[Why?](https://framagit.org/dCF/deCloudflare/-/blob/master/readme/en.md)*

Tag Brave search results pointing to Cloudflare-protected websites.

| [Install](../../../../raw/branch/master/braveSearchCloudflareDetector/main.user.js) |
|-------------------------------------------------------------------------------------|

![](./screenshot.png)

## Changelog

* `0.1.0` (2022-12-10) • [Initial release](https://git.kaki87.net/KaKi87/userscripts/src/commit/42206699efeeda41852b1d9fe234d608405f0a7d/braveSearchCloudflareDetector)
* `0.1.1` (2023-01-03) • [Fix support for KaKi87/deno-api@e118d4d](https://git.kaki87.net/KaKi87/userscripts/src/commit/9233234ec6bffeaa2ab663e4f4144a3615bf93a6/braveSearchCloudflareDetector)
* `0.1.2` (2023-01-04) • [Fix 9233234](https://git.kaki87.net/KaKi87/userscripts/src/commit/0a179fb0eb3d4f10ab0de79ba0cba9484ddd46bb/braveSearchCloudflareDetector)