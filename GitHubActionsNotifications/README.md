[← Homepage](../README.md)

# GitHub Actions notifications

Enable desktop notifications for GitHub Actions updates.

| [Install](../../../../raw/branch/master/GitHubActionsNotifications/main.user.js) |
|----------------------------------------------------------------------------------|

## DEPRECATION NOTICE

This project is now deprecated in favor of [GitHub Actions Enhanced](../GitHubActionsEnhanced).

---

![](./screenshot.jpg)

## Changelog

* `1.0.0` (2020-12-10) • [Initial release](https://git.kaki87.net/KaKi87/userscripts/src/commit/02aec15a980dd0f5bfac31a8e83daf9b713792ab/GitHubActionsNotifications)
* `1.0.1` (2021-01-21) • [Fix action failure detection due to GitHub website update](https://git.kaki87.net/KaKi87/userscripts/src/commit/b12d38660a5cc4bf1eecbcca3adb13320acd4abe/GitHubActionsNotifications)
* `1.0.2` (2021-01-21) • [Fix action success detection due to GitHub website update](https://git.kaki87.net/KaKi87/userscripts/src/commit/dfbfc38fb7901ae6d71d0382faa2405b54d81389/GitHubActionsNotifications)