**English** | [日本語](https://greasyfork.org/ja/scripts/406420) | [中文](https://greasyfork.org/zh-CN/scripts/406420)  

**How to install UserScripts:**  
[https://greasyfork.org/en/help/installing-user-scripts](https://greasyfork.org/en/help/installing-user-scripts)

**Functions of this script:**  
It displays thumbnail images instead of auto-playing videos on channel pages, individual video pages, etc.  
You can play the thumbnail videos by mouse click or [Space] key.  
(Advertisements and real-time live streamings would automatically start normally)  
![](https://greasyfork.s3.us-east-2.amazonaws.com/jupz1gzdujy8vo32plgs2leq3d41) ![](https://greasyfork.s3.us-east-2.amazonaws.com/fb9vu5jscacizjcqyfz9h3cbgnyd)  
Autoplay is enabled if the "AUTOPLAY" switch is turned on and you move to the "Up next" video.  
Added a dedicated "AUTOPLAY" switch to the playlist.  
![](https://greasyfork.s3.us-east-2.amazonaws.com/qdcs56hanfmj7k3ke47ivow5gs5x)  
Less bold bottom shadow of the player, especially on the thumbnail.  
![](https://greasyfork.s3.us-east-2.amazonaws.com/isnahow3fjj9wu247f5fe9jx0280)

Try on YouTube:  
[https://www.youtube.com/](https://www.youtube.com/)

**Donate for this script:**
- [PayPal.Me/kantankikaku](https://paypal.me/kantankikaku) ←You can send me $1 🤗
- [Amazon Wish List](https://www.amazon.co.jp/hz/wishlist/ls/HB10OXAOQ3D7?language=en_US&tag=knoa-22) ←Snacks while coding 😋
- [Amazon CPU Tamer](https://greasyfork.org/en/scripts/415464-amazon-cpu-tamer) ←Enjoy shopping as well 🥳
- Any kind of referral links are also appreciated 🎉

**List of my scripts:**
- [YouTube scripts](https://greasyfork.org/en/scripts?set=361295)
- [All scripts](https://greasyfork.org/en/users/78927-knoa?sort=name)

**Twitter:**  
[@kantankikaku](https://twitter.com/kantankikaku/)  
[#YouTubeClickToPlay](https://twitter.com/hashtag/YouTubeClickToPlay)

**Keywords:**  
Chrome, Firefox, Edge, Safari, Greasemonkey, Tampermonkey, Violentmonkey, UserScript, Extension, Add-on  
kill prevent avoid disable turn off stop pause block autoplay autostart auto automatically play start  
channel home top user video page show thumbnail poster cover image  
force enable require need have to click tap touch space key settings preferences configs option  
Chrome flags policy Firefox media.autoplay.default playing sound audio browser permissions

**Screenshots:**

[![](https://greasyfork.s3.us-east-2.amazonaws.com/foshcwhw47jr9iegrpc27d22n13n)](https://greasyfork.s3.us-east-2.amazonaws.com/jupz1gzdujy8vo32plgs2leq3d41)
[![](https://greasyfork.s3.us-east-2.amazonaws.com/ma1zoza8vj7pgalepex80kt6dk9t)](https://greasyfork.s3.us-east-2.amazonaws.com/fb9vu5jscacizjcqyfz9h3cbgnyd)
[![](https://greasyfork.s3.us-east-2.amazonaws.com/exa87zjhi3u5jwu8rgssxgqrdxsy)](https://greasyfork.s3.us-east-2.amazonaws.com/qdcs56hanfmj7k3ke47ivow5gs5x)
[![](https://greasyfork.s3.us-east-2.amazonaws.com/8runbuuentublmebyowls110wbpm)](https://greasyfork.s3.us-east-2.amazonaws.com/isnahow3fjj9wu247f5fe9jx0280)