// ==UserScript==
// @match         https://old.reddit.com/*
// @exclude-match https://old.reddit.com/chat/*
// @name          Reddit Old Better Editor
// @description   Syntax highlighting & preview for Reddit Old post & comment editor
// @version       0.1.1
// @author        KaKi87
// @license       MIT
// @namespace     https://git.kaki87.net/KaKi87/userscripts/src/branch/master/redditOldBetterEditor
// @require       https://git.kaki87.net/KaKi87/userscripts/raw/commit/0da5a1c5d535fb40588d1c421170fe79790e7724/_lib/waitForSelector.js
// ==/UserScript==

const
    APP_ID = 'redditOldBetterEditor',
    appElements = [];

(async () => {
    const [
        codemirror,
        codemirrorMarkdown,
        codemirrorTheme,
        snudown
    ] = await Promise.all([
        import('https://jspm.dev/codemirror'),
        import('https://jspm.dev/@codemirror/lang-markdown'),
        import('https://jspm.dev/cm6-theme-basic-dark'),
        import('https://cdn.jsdelivr.net/npm/snudown-js@4.0.0/+esm')
    ]);
    waitForSelector(
        'textarea',
        textareaElement => {
            textareaElement.classList.add(`__${APP_ID}__editorContainer__textarea`);
            const editorContainer = textareaElement.parentElement;
            editorContainer.classList.add(`__${APP_ID}__editorContainer`);
            {
                const
                    editorElement = document.createElement('div'),
                    previewElement = document.createElement('div');
                editorElement.classList.add(`__${APP_ID}__`, `__${APP_ID}__editorContainer__editor`);
                editorContainer.appendChild(editorElement);
                appElements.push(editorElement);
                previewElement.classList.add(`__${APP_ID}__`, `__${APP_ID}__editorContainer__preview`);
                editorContainer.appendChild(previewElement);
                appElements.push(previewElement);
                new codemirror.EditorView({
                    doc: textareaElement.value,
                    extensions: [
                        codemirror.basicSetup,
                        codemirrorMarkdown.markdown(),
                        codemirrorTheme.basicDark,
                        codemirror.EditorView.updateListener.of(event => {
                            const value = event.state.doc.toString();
                            textareaElement.value = value;
                            previewElement.innerHTML = snudown.markdown(value);
                        })
                    ],
                    parent: editorElement
                });
            }
            {
                const previewToggleElement = document.createElement('button');
                previewToggleElement.classList.add(`__${APP_ID}__`, `__${APP_ID}__editorContainer__previewToggle`);
                previewToggleElement.textContent = 'Show preview';
                previewToggleElement.setAttribute('type', 'button');
                previewToggleElement.addEventListener(
                    'click',
                    () => {
                        const isPreview = editorContainer.classList.toggle(`__${APP_ID}__editorContainer--preview`);
                        previewToggleElement.textContent = isPreview ? 'Hide preview' : 'Show preview';
                    }
                );
                editorContainer.appendChild(previewToggleElement);
                appElements.push(previewToggleElement);
            }
            for(const element of document.querySelectorAll(`.__${APP_ID}__`)){
                if(!appElements.includes(element))
                    element.remove();
            }
            return true;
        }
    ).catch(console.error);
})();

{
    const styles = new CSSStyleSheet();
    styles.replaceSync(`
        .__${APP_ID}__editorContainer {
            position: relative;
            color-scheme: dark;
        }
        .__${APP_ID}__editorContainer__textarea {
            visibility: hidden;
        }
        .__${APP_ID}__editorContainer__editor,
        .__${APP_ID}__editorContainer__preview {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
        .__${APP_ID}__editorContainer--preview .__${APP_ID}__editorContainer__editor {
            display: none;
        }
        .__${APP_ID}__editorContainer__editor .cm-editor {
            height: 100%;
        }
        .__${APP_ID}__editorContainer__preview {
            display: none;
            overflow: auto;
        }
        .__${APP_ID}__editorContainer--preview .__${APP_ID}__editorContainer__preview {
            display: block;
        }
        .__${APP_ID}__editorContainer__previewToggle {
            z-index: 1;
            position: absolute;
            top: 0;
            right: 0;
            transform: translateX(calc(100% + 10px));
        }
    `);
    document.adoptedStyleSheets = [...document.adoptedStyleSheets, styles];
}