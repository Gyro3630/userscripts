// ==UserScript==
// @match       https://*/*
// @name        Submit form on Ctrl+Enter
// @description Submit any form on any website using Ctrl+Enter
// @version     0.1.0
// @author      KaKi87
// @license     MIT
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/submitFormOnCtrlEnter
// ==/UserScript==

window.addEventListener(
    'keydown',
    event => {
        if(
            event.ctrlKey
            &&
            event.key === 'Enter'
            &&
            [
                'INPUT',
                'SELECT',
                'TEXTAREA'
            ].includes(event.target.tagName)
        ){
            const submitTimeout = setTimeout(
                () => HTMLFormElement.prototype.submit.call(event.target.form),
                100
            );
            event.target.form.addEventListener(
                'submit',
                () => clearTimeout(submitTimeout)
            );
            window.addEventListener(
                'beforeunload',
                () => clearTimeout(submitTimeout)
            );
        }
    },
    {
        capture: true
    }
);