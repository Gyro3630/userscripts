[← Homepage](../README.md)

# Enhancements for Dealabs

Filtering enhancements for Dealabs.com

| [Install](../../../../raw/branch/master/dealabsEnhanced/main.user.js) |
|-----------------------------------------------------------------------|

![](./screenshot.png)

## Changelog

* `0.1.0` (2023-01-12) • [Initial release](https://git.kaki87.net/KaKi87/userscripts/src/commit/100597f2c0db815fd0292b1900a03f7dd8c00281/dealabsEnhanced)
* `0.1.1` (2023-01-12) • [Fix settings desktop positioning](https://git.kaki87.net/KaKi87/userscripts/src/commit/87622d98b2b92bda4d430cc984f4d99773945a4b/dealabsEnhanced)