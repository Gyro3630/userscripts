[← Homepage](../README.md)

# Mastodon Redirector

Redirect any Mastodon app to your favourite one.

| [Install](../../../../raw/branch/master/mastodonRedirector/main.user.js) |
|--------------------------------------------------------------------------|

## DEPRECATION NOTICE

This project is now deprecated in favor of [Fediverse redirector](../fediverseRedirector/README.md).

---

To set your favourite instance, click on the *Update settings* userscript menu :

![](./screenshot.png)

Then, submit your instance URL, the userscript will detect the app and confirm.

Supported apps : [Mastodon](https://github.com/mastodon/mastodon), [Elk](https://github.com/elk-zone/elk).

## Changelog

* `0.1.0` (2023-01-19) • [Initial release](https://git.kaki87.net/KaKi87/userscripts/src/commit/0615f9f4ae2d68f464b00887e4a33305f5fc73a0/mastodonRedirector)
* `0.1.1` (2023-01-23) • [Fix 'update settings' menu on mobile](https://git.kaki87.net/KaKi87/userscripts/src/commit/92980bc00b227c9a37a279bdab04bb822ebacbc0/mastodonRedirector)