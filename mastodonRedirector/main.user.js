// ==UserScript==
// @match       https://*/*
// @name        Mastodon Redirector
// @description Redirect any Mastodon app to your favourite one
// @grant       GM.getValue
// @grant       GM.setValue
// @grant       GM.registerMenuCommand
// @grant       GM.xmlHttpRequest
// @version     0.1.2
// @author      KaKi87
// @license     MIT
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/mastodonRedirector
// @run-at      document-start
// ==/UserScript==

const
    DEFAULT_APP = 'Elk',
    DEFAULT_APP_HOSTNAME = 'main.elk.zone';

let isLoaded;

window.addEventListener(
    'DOMContentLoaded',
    () => isLoaded = true
);

const
    getApp = document => document.querySelector('#mastodon')
        ? 'Mastodon'
        : document.querySelector('meta[property="og:site_name"][content="Elk"]')
            ? 'Elk'
            : undefined,
    getSettings = async () => ({
        app: await GM.getValue('app') || DEFAULT_APP,
        appHostname: await GM.getValue('appHostname') || DEFAULT_APP_HOSTNAME
    }),
    main = async () => {
        const originApp = getApp(document);
        if(originApp){
            const
                originAppHostname = window.location.hostname,
                {
                    app,
                    appHostname
                } = await getSettings();
            if(
                originApp === app
                &&
                originAppHostname === appHostname
            ) return;
            let
                instanceHostname,
                profileId,
                postId,
                url;
            switch(originApp){
                case 'Mastodon': {
                    instanceHostname = originAppHostname;
                    [
                        ,
                        profileId,
                        postId
                    ] = window.location.pathname.match(/^\/@(\w+)(?:\/([0-9]+))?$/) || [];
                    break;
                }
                case 'Elk': {
                    [
                        ,
                        instanceHostname,
                        profileId,
                        postId
                    ] = window.location.pathname.match(/^\/([\w.]+)\/@(\w+)(?:\/([0-9]+))?$/) || [];
                    break;
                }
            }
            if(!profileId) return;
            switch(app){
                case 'Mastodon': {
                    url = `https://${appHostname}/@${profileId}@${instanceHostname}${postId ? `/${postId}` : ''}`;
                    break;
                }
                case 'Elk': {
                    url = `https://${appHostname}/${instanceHostname}/@${profileId}${postId ? `/${postId}` : ''}`;
                    break;
                }
            }
            window.stop();
            window.location.replace(url);
        }
        else if(!isLoaded)
            setTimeout(main, 0);
    };

main().catch(console.error);

GM.registerMenuCommand(
    'Update settings',
    async () => {
        await new Promise(resolve => setTimeout(resolve, 1));
        const
            {
                app,
                appHostname
            } = await getSettings(),
            newAppHostname = prompt(
                'App hostname',
                appHostname
            );
        let
            newApp;
        if(appHostname === newAppHostname)
            newApp = app;
        else {
            const document = await new Promise(resolve => GM.xmlHttpRequest({
                url: `https://${newAppHostname}`,
                responseType: 'document',
                onload: ({ response }) => resolve(response),
                onerror: () => resolve()
            }));
            if(document)
                newApp = getApp(document);
        }
        if(newApp){
            await GM.setValue('app', newApp);
            await GM.setValue('appHostname', newAppHostname);
            alert(`Settings updated.\nApp : ${newApp}\nApp hostname : ${newAppHostname}`);
        }
        else
            alert('Error : could not identify app.\nSupported apps : Mastodon, Elk');
    }
);