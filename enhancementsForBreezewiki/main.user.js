// ==UserScript==
// @match         *://*/*
// @name          Enhancements for BreezeWiki
// @description   Enhancements for BreezeWiki
// @grant         GM.setClipboard
// @version       0.1.2
// @author        KaKi87
// @license       MIT
// @namespace     https://git.kaki87.net/KaKi87/userscripts/src/branch/master/enhancementsForBreezewiki
// ==/UserScript==

if(document.querySelector('.fandom-community-header__background')){
    const
        url = new URL(window.location),
        homeElement = document.createElement('a'),
        copyElement = document.createElement('a'),
        styles = new CSSStyleSheet();
    homeElement.textContent = '🏠';
    homeElement.setAttribute('href', `/${new URL(window.location).pathname.split('/')[1]}`);
    homeElement.setAttribute('title', 'Go home');
    document.querySelector('.page__main').insertAdjacentElement('afterbegin', homeElement);
    if(url.pathname.endsWith('/search')){
        let [
            , seasonNumber
            , episodeNumber
        ] = url.searchParams.get('q').match(/^s([0-9]+)e([0-9]+)$/i) || [];
        if(seasonNumber && episodeNumber){
            seasonNumber = parseInt(seasonNumber);
            episodeNumber = parseInt(episodeNumber);
            import('https://cdn.jsdelivr.net/npm/number-to-words/+esm').then(({ 'default': { toWordsOrdinal } }) => {
                const
                    suggestionElement = document.createElement('p'),
                    suggestedQuery = `${toWordsOrdinal(episodeNumber)}${seasonNumber === episodeNumber ? '' : ` ${toWordsOrdinal(seasonNumber)}`}`;
                suggestionElement.innerHTML = `Try <a href="?q=${encodeURIComponent(suggestedQuery)}">${suggestedQuery}</a>`;
                document.querySelector('#content').insertAdjacentElement('beforebegin', suggestionElement);
            });
        }
    }
    copyElement.textContent = '📄';
    copyElement.setAttribute('href', '#');
    copyElement.setAttribute('title', 'Copy title');
    copyElement.addEventListener('click', event => {
        event.preventDefault();
        GM.setClipboard(document.querySelector('.page-title').textContent);
    });
    homeElement.insertAdjacentElement('afterend', copyElement);
    // language=CSS
    styles.replaceSync(`
        :root {
            color-scheme: dark;
        }
        .page {
            margin: 0;
        }
        .page__main {
            border-radius: 0;
        }
        .article-table {
            display: block;
            overflow-x: auto;
        }
    `);
    document.adoptedStyleSheets = [...document.adoptedStyleSheets, styles];
}