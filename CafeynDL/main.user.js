// ==UserScript==
// @match       https://cdn.cafeyn.co/reader.html*
// @name        Downloader for Cafeyn
// @description Allow downloading Cafeyn documents as PDF
// @version     0.1.0
// @author      KaKi87
// @license     WTFPL
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/CafeynDL
// @run-at      document-start
// @require     https://cdn.jsdelivr.net/npm/slug@5.3.0/slug.js
// @require     https://cdn.jsdelivr.net/npm/pdf-lib@1.17.1/dist/pdf-lib.min.js
// @require     https://git.kaki87.net/KaKi87/userscripts/raw/commit/4aa51028f61a7251ffb9f89839a844acffe3d108/_lib/createFetchInterceptor.js
// @require     https://git.kaki87.net/KaKi87/userscripts/raw/commit/0da5a1c5d535fb40588d1c421170fe79790e7724/_lib/waitForSelector.js
// ==/UserScript==

const createCanvas = (width, height) => {
    const
        canvas = document.createElement('canvas'),
        canvasContext = canvas.getContext('2d');
    canvas.width = width;
    canvas.height = height;
    return {
        canvas,
        canvasContext
    };
};

/**
 * @param {string} url
 * @returns {Promise<HTMLImageElement>}
 */
const fetchImage = url => new Promise(resolve => {
    const image = new Image();
    image.addEventListener('load', () => resolve(image));
    image.src = url;
});

const rotateImage = async image => {
    const {
        canvas,
        canvasContext
    } = createCanvas(
        image.width,
        image.height
    );
    canvasContext.translate(canvas.width / 2, canvas.height / 2);
    canvasContext.rotate(Math.PI / 2);
    canvasContext.drawImage(image, -image.width / 2, -image.height / 2);
    canvasContext.rotate(-Math.PI / 2);
    canvasContext.translate(-canvas.width / 2, -canvas.height / 2);
    return await fetchImage(canvas.toDataURL());
};

const flipImage = async (image, direction) => {
    const {
        canvas,
        canvasContext
    } = createCanvas(
        image.width,
        image.height
    );
    canvasContext.scale(
        {
            'horizontal': -1,
            'vertical': 1
        }[direction],
        {
            'horizontal': 1,
            'vertical': -1
        }[direction]
    );
    canvasContext.drawImage(
        image,
        {
            'horizontal': -image.width,
            'vertical': 0
        }[direction],
        {
            'horizontal': 0,
            'vertical': -image.height
        }[direction],
        image.width,
        image.height
    );
    return await fetchImage(canvas.toDataURL());
};

const transformImage = async ({
    image,
    rotateCount,
    flipDirection
}) => {
    for(let i = 0; i < rotateCount; i++)
        image = await rotateImage(image);
    if(flipDirection)
        image = await flipImage(image, flipDirection);
    return image;
};

const showSaveDialog = (name, blob) => {
    const element = document.createElement('a');
    element.setAttribute('download', name);
    element.setAttribute('href', URL.createObjectURL(blob));
    element.click();
};

{
    const { fetch } = unsafeWindow;
    unsafeWindow.fetch = createFetchResponseInterceptor({
        fetch,
        onResponse: async ({
            url,
            type,
            data
        }) => {
            if(url.startsWith('https://reader-api.cafeyn.co/ticket/') && url.endsWith('/material.json')){
                if(type === 'text')
                    data = JSON.parse(data);
                const
                    fileName = `${slug(data['metadata']['title'])}_${data['metadata']['publication_date']}.pdf`,
                    pages = data['pages']
                        .map(item1 => ({
                            rowCount: item1['hd']['tile_row_count'],
                            columnCount: item1['hd']['tile_col_count'],
                            tileWidth: item1['hd']['tile_width'],
                            tileHeight: item1['hd']['tile_height'],
                            tiles: Object.entries(item1['hd']['transformations'])
                                .map(([item2, item3]) => ({
                                    url: `https://cdn.cafeyn.co/${url.slice(29, -14)}/${item1['hd']['path']}/${item2}`,
                                    rotateCount: item3[0],
                                    isFlip: !!item3[1]
                                }))
                        })),
                    totalTileCount = pages.reduce((tileCount, { tiles }) => tileCount + tiles.length, 0),
                    toolbarElement = await waitForSelector('#id4'),
                    buttonContainerElement = document.createElement('div'),
                    buttonElement = document.createElement('a'),
                    buttonProgressContainerElement = document.createElement('span'),
                    buttonProgressElement = document.createElement('span'),
                    buttonTextElement = document.createElement('span');
                let isLoading = false;
                buttonElement.addEventListener('click', async event => {
                    event.preventDefault();
                    if(isLoading) return;
                    isLoading = true;
                    buttonTextElement.textContent = 'Downloading';
                    const pdf = await PDFLib.PDFDocument.create();
                    {
                        let totalTileIndex = 0;
                        for(let pageIndex = 0; pageIndex < pages.length; pageIndex++){
                            const
                                page = pages[pageIndex],
                                {
                                    canvas: pageCanvas,
                                    canvasContext: pageCanvasContext
                                } = createCanvas(
                                    page.tileWidth * page.columnCount,
                                    page.tileHeight * page.rowCount
                                );
                            let pageWidth = 0,
                                pageHeight = 0;
                            {
                                let tileIndex = 0;
                                for(let rowIndex = 0; rowIndex < page.rowCount; rowIndex++){
                                    for(let columnIndex = 0; columnIndex < page.columnCount; columnIndex++){
                                        const tile = page.tiles[tileIndex];
                                        if(tile){
                                            const image = await fetchImage(tile.url);
                                            pageCanvasContext.drawImage(
                                                (tile.rotateCount || tile.isFlip)
                                                    ? await transformImage({
                                                        image,
                                                        rotateCount: tile.rotateCount,
                                                        flipDirection: tile.isFlip
                                                            ? {
                                                                0: 'vertical',
                                                                1: 'horizontal',
                                                                2: 'vertical',
                                                                3: 'horizontal'
                                                            }[tile.rotateCount]
                                                            : undefined
                                                    })
                                                    : image,
                                                columnIndex * page.tileWidth,
                                                rowIndex * page.tileHeight,
                                                image.width,
                                                image.height
                                            );
                                            if(rowIndex === 0)
                                                pageWidth += image.width;
                                            if(columnIndex === 0)
                                                pageHeight += image.height;
                                            tileIndex++;
                                            totalTileIndex++;
                                            buttonProgressElement.style['width'] = `${totalTileIndex / totalTileCount * 100}%`;
                                        }
                                    }
                                }
                            }
                            {
                                const {
                                    canvas: croppedPageCanvas,
                                    canvasContext: croppedPageCanvasContext
                                } = createCanvas(pageWidth, pageHeight);
                                croppedPageCanvasContext.drawImage(
                                    pageCanvas,
                                    0, 0,
                                    pageWidth,
                                    pageHeight,
                                    0, 0,
                                    pageWidth,
                                    pageHeight
                                );
                                pdf.addPage([pageWidth, pageHeight]).drawImage(
                                    await pdf.embedPng(croppedPageCanvas.toDataURL('image/png')),
                                    {
                                        x: 0,
                                        y: 0,
                                        width: pageWidth,
                                        height: pageHeight
                                    }
                                );
                            }
                        }
                    }
                    buttonTextElement.textContent = 'Saving';
                    await new Promise(resolve => setTimeout(resolve, 0));
                    showSaveDialog(fileName, new Blob([await pdf.save()]));
                    buttonTextElement.textContent = 'Download';
                    buttonProgressElement.style['width'] = '0';
                    isLoading = false;
                });
                buttonTextElement.textContent = 'Download';
                buttonTextElement.classList.add('CAFEYN-DL__buttonContainer__button__text');
                buttonProgressElement.classList.add('CAFEYN-DL__buttonContainer__button__progressContainer__progress');
                buttonProgressContainerElement.appendChild(buttonProgressElement);
                buttonProgressContainerElement.classList.add('CAFEYN-DL__buttonContainer__button__progressContainer');
                buttonElement.appendChild(buttonTextElement);
                buttonElement.appendChild(buttonProgressContainerElement);
                buttonElement.setAttribute('role', 'button');
                buttonElement.setAttribute('href', '#');
                buttonElement.classList.add('CAFEYN-DL__buttonContainer__button');
                buttonContainerElement.appendChild(buttonElement);
                buttonContainerElement.classList.add('CAFEYN-DL__buttonContainer');
                toolbarElement.appendChild(buttonContainerElement);
            }
        }
    });
}

{
    const styleElement = document.createElement('style');
    styleElement.appendChild(document.createTextNode(`
        .CAFEYN-DL__buttonContainer {
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .CAFEYN-DL__buttonContainer__button {
            text-decoration: none;
            z-index: 51;
            width: 200px;
            height: 75%;
            background-color: rgba(255, 255, 255, 0.1);
            transition: background-color 0.25s;
            display: flex;
            flex-direction: column;
            position: relative;
        }
        .CAFEYN-DL__buttonContainer__button:hover {
            background-color: rgba(255, 255, 255, 0.25);
        }
        .CAFEYN-DL__buttonContainer__button__progressContainer {
            position: absolute;
            left: 0;
            bottom: 0;
            width: 100%;
            height: 3px;
        }
        .CAFEYN-DL__buttonContainer__button__progressContainer__progress {
            display: block;
            width: 0;
            height: 100%;
            background-color: white;
        }
        .CAFEYN-DL__buttonContainer__button__text {
            font-family: sans-serif;
            color: white;
            text-transform: uppercase;
            flex: 1;
            display: flex;
            align-items: center;
            justify-content: center;
        }
    `));
    document.head.appendChild(styleElement);
}