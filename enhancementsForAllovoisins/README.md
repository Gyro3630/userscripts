[← Homepage](../README.md)

# Enhancements for AlloVoisins

| [Install](../../../../raw/branch/master/enhancementsForAllovoisins/main.user.js) |
|----------------------------------------------------------------------------------|

## Fonctionnalités

- Ré-authentification automatique
- Masquage des éléments indésirables
  - "Remontez à la Une"
  - "Complétez votre profil"
  - "Passez à la vitesse supérieure"
  - Publications sponsorisées
  - Demandes privées
  - Paramètres pro
  - Badge de profil "incomplet"
  - "Encore plus pratique sur l'app !"
- Onglet de profil "avis laissés" *(apparait après chargement de l'onglet "activité")*
- Ouverture du profil depuis la messagerie dans un nouvel onglet 

## Notes de versions

`0.1.0` (19/11/2023) • Publication initiale
`0.1.1` (28/11/2023) • Ajout de la ré-authentification automatique
`0.1.2` (28/12/2023) • Masquage d'élément indésirable
`0.1.3` (02/01/2024) • Correction de l'ouverture du profil depuis la messagerie dans un nouvel onglet 