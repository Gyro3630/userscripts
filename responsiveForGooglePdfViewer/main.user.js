// ==UserScript==
// @match         https://docs.google.com/viewerng/viewer?url=*
// @name          Responsive design for Google PDF Viewer
// @description   Responsive design for Google PDF Viewer
// @version       0.1.0
// @author        KaKi87
// @license       MIT
// @namespace     https://git.kaki87.net/KaKi87/userscripts/src/branch/master/responsiveForGooglePdfViewer
// ==/UserScript==

{
    const metaViewportElement = document.createElement('meta');
    metaViewportElement.setAttribute('name', 'viewport');
    metaViewportElement.setAttribute('content', 'width=device-width, initial-scale=1');
    document.head.appendChild(metaViewportElement);
}

{
    const styles = new CSSStyleSheet();
    // language=CSS
    styles.replaceSync(`
        .drive-viewer {
            display: none;
        }
    `);
    document.adoptedStyleSheets = [...document.adoptedStyleSheets, styles];
}