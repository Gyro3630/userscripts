// ==UserScript==
// @match       https://*.proton.me/*
// @name        ProtonMail without annoyances
// @description Remove annoying ProtonMail buttons & banners
// @grant       GM.addStyle
// @version     0.1.2
// @author      KaKi87
// @license     GPL-3.0-or-later
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/protonmailWithoutAnnoyances
// ==/UserScript==

{
    GM.addStyle(`
        .bg-promotion {
            display: none;
        }
    `);
    GM.addStyle(`
        .topnav-listItem:has([href*="/upgrade"]),
        .topnav-listItem:has([href*="/upgrade"]) + .topnav-vr,
        .topnav-listItem:has([data-testid^="cta"]),
        .dropdown-item:has([href="https://shop.proton.me"]),
        .navigation-item:has([href*="/upgrade"]) {
            display: none;
        }
    `);
}